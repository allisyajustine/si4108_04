package com.firebase.eatciw.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.firebase.eatciw.adapter.DaftarPromoAdapter;
import com.firebase.eatciw.model.PromoModel;
import com.firebase.eatciw.promo1;
import com.firebase.eatciw.promo2;
import com.firebase.eatciw.promo3;
import com.firebase.eatciw.promo_list;
import com.firebase.eatciw.util.DarkMode;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromoFragment extends Fragment {

    public static PromoFragment newInstance() {
        PromoFragment fragment = new PromoFragment();
        return fragment;
    }

    Context context;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context = context;
    }

    private TextView tblTambah;
    private DatePickerDialog datePickerDialog;
    private DaftarPromoAdapter daftarPromoAdapter;
    private ArrayList<PromoModel> promoList;
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private String tgl;
    private SimpleDateFormat simpleDateFormat;

    @SuppressLint("SimpleDateFormat")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_promo, container, false);
        tblTambah = view.findViewById(R.id.btnTambah);
        recyclerView = view.findViewById(R.id.recycler);
        progressBar = view.findViewById(R.id.progress);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String statusLogin = sharedPreferences.getString("status", "");
        String statusDark = sharedPreferences.getString("statusDark", "");
        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(context, view, statusDark);


        simpleDateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");

        promoList = new ArrayList<>();
        daftarPromoAdapter = new DaftarPromoAdapter(context, promoList);
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(daftarPromoAdapter);

        //mengambil data login dan jika dia bukan admin tombol tambah akan hilang
        if (!statusLogin.isEmpty() && statusLogin.equals("admin")){
            tblTambah.setVisibility(View.VISIBLE);
        }else{
            tblTambah.setVisibility(View.GONE);
        }
        tblTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogTambah = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogTambah.setContentView(R.layout.edit_promo_dialog);
                dialogTambah.setCancelable(true);
                dialogTambah.show();

                final TextView tanggal = dialogTambah.findViewById(R.id.tanggalBerlaku);
                TextView addTgl = dialogTambah.findViewById(R.id.addTanggal);
                final EditText jumlah = dialogTambah.findViewById(R.id.edtJumlah);
                final EditText deskripsi = dialogTambah.findViewById(R.id.edtDeskripsi);
                final Spinner status = dialogTambah.findViewById(R.id.spinnerStatus);
                TextView btnProses = dialogTambah.findViewById(R.id.btnAksi);

                tanggal.setText(null);
                final Calendar newCal = Calendar.getInstance();
                addTgl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(i, i1, i2);
                                tgl = simpleDateFormat.format(calendar.getTime());
                                Log.e("tanggal terpilih", tgl + " asa");
                                tanggal.setText(simpleDateFormat.format(calendar.getTime()));;
                            }
                        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_WEEK));
                        datePickerDialog.show();
                    }
                });


                btnProses.setText("Tambah promo");
                btnProses.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String tanggalTerpilih = tanggal.getText().toString();
                        String jumlahPotong = jumlah.getText().toString();
                        String deskripsiPromo = deskripsi.getText().toString();
                        String statusPromo = status.getSelectedItem().toString();

                        if(!tanggalTerpilih.isEmpty() && !jumlahPotong.isEmpty() && !deskripsiPromo.isEmpty()){
                            if(!statusPromo.equals("-Pilih status promo-")){
                                prosesTambahPromo(tanggalTerpilih, jumlahPotong, deskripsiPromo, statusPromo, dialogTambah);
                            }else{
                                Toast.makeText(context, "Mohon tentukan status promo", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });


        dataPromo();
        return view;
    }

    private void dataPromo(){

        databaseReference.child("datapromo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);
                promoList.clear();
                for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    String keyPromo = dataSnapshot1.getKey();
                    String kodePromo = dataSnapshot1.child("kodePromo").getValue().toString();
                    String tanggalBerlaku = dataSnapshot1.child("tanggalBerlaku").getValue().toString();
                    String jumlahPemotongan = dataSnapshot1.child("jumlahPemotongan").getValue().toString();
                    String deskripsiPromo = dataSnapshot1.child("deskripsiPromo").getValue().toString();
                    String statusPromo = dataSnapshot1.child("statusPromo").getValue().toString();

                    PromoModel promoModel = new PromoModel();
                    promoModel.setIdPromo(keyPromo);
                    promoModel.setKodePromo(kodePromo);
                    promoModel.setTanggalPromo(tanggalBerlaku);
                    promoModel.setHartaPemotonganPromo(jumlahPemotongan);
                    promoModel.setDeskripsiPromo(deskripsiPromo);
                    promoModel.setStatusPromo(statusPromo);
                    promoList.add(promoModel);
                }
                daftarPromoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void prosesTambahPromo(String tanggal, String jumlahPotong, String des, String statusPr, final Dialog dialogTambah){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        String uid = UUID.randomUUID().toString().substring(0, 8).toUpperCase();//Membuat kode promo random
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("kodePromo", uid);
        hashMap.put("tanggalBerlaku", tanggal);
        hashMap.put("jumlahPemotongan", jumlahPotong);
        hashMap.put("deskripsiPromo", des);
        hashMap.put("statusPromo", statusPr);

        databaseReference.child("datapromo").push().setValue(hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogTambah.dismiss();
                Toast.makeText(context, "Data promo ditambah", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(context, "Gagal menambah data promo " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
