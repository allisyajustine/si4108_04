package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.eatciw.data.ProductDummy;
import com.firebase.eatciw.data.model.Product;

import static com.firebase.eatciw.makanan1.KEY_ITEM;

public class Menu3 extends AppCompatActivity implements ProductAdapter.Interaction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu3);

        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setAdapter(new ProductAdapter(
                this, ProductDummy.getAllSnacks()
        ));
    }

    public void hihi(View view) {
        Intent intent = new Intent(Menu3.this, jajanan1.class);
        startActivity(intent);
    }

    public void yiyi(View view) {
        Intent intent = new Intent(Menu3.this, jajanan2.class);
        startActivity(intent);
    }

    @Override
    public void onClick(Product item) {
        Intent intent = new Intent(Menu3.this, makanan1.class);
        intent.putExtra(KEY_ITEM, item);
        startActivity(intent);
    }
}
