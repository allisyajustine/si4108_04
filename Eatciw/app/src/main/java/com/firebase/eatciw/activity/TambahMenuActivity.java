package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.R;
import com.firebase.eatciw.model.MenuModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class TambahMenuActivity extends AppCompatActivity {

    TextView kembali, btnUpload, btnTambah;
    ImageView gambarMenu;
    EditText namaMenu, hargaMenu, deskripsiMenu;
    Spinner jenisMenu;
    Context context = TambahMenuActivity.this;
    DatabaseReference databaseReference;
    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST=1;
    int bitmap_size = 70;
    StorageReference storageReference;
    Uri pathFile;
    String cekGambar;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_menu);
        kembali = findViewById(R.id.backButton);
        btnUpload = findViewById(R.id.btnAddGambar);
        btnTambah = findViewById(R.id.btnAddMenu);
        gambarMenu = findViewById(R.id.menuGambar);
        namaMenu = findViewById(R.id.edtNamaMenu);
        hargaMenu = findViewById(R.id.edtHarga);
        deskripsiMenu = findViewById(R.id.edtDeskripsi);
        jenisMenu = findViewById(R.id.edtJenis);
        btnTambah.setEnabled(false);
        btnTambah.setBackgroundColor(getResources().getColor(R.color.notselect));
        btnTambah.setTextColor(getResources().getColor(R.color.notselecttext));
        btnTambah.setText("Mohon tambah gambar");

        databaseReference = FirebaseDatabase.getInstance().getReference("menu");
        storageReference = FirebaseStorage.getInstance().getReference();


        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihGambar();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = namaMenu.getText().toString();
                String harga = hargaMenu.getText().toString();
                String jenis = jenisMenu.getSelectedItem().toString();
                String deskripsi = deskripsiMenu.getText().toString();

                if(!nama.isEmpty() && !harga.isEmpty() && !jenis.isEmpty() && !deskripsi.isEmpty()){
                    if(nama.length() >= 4){
                        if (harga.length() >= 4){
                            if (deskripsi.length() >= 18){
                                tambahMenu(nama, harga, jenis, deskripsi);
                            }else{
                                Toast.makeText(context, "Panjang deskripsi minimal 18 karakter", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, "Minimal harga Rp. 1.000", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(context, "Panjang nama minimal 4 karakter", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Masih ada kolom kosong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void tambahMenu(final String nama, final String harga, final String jenis, final String deskripsi){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        if (pathFile != null) {
            gambarMenu.setDrawingCacheEnabled(true);
            gambarMenu.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) gambarMenu.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] data = baos.toByteArray();
            final StorageReference storageReference2nd = storageReference.child("menudata/" + System.currentTimeMillis() + "." + "jpg");
            final UploadTask uploadTask =  storageReference2nd.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    dialog.dismiss();
                    Toast.makeText(context, "gagal ditambah" + exception.getMessage(), Toast.LENGTH_LONG).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();

                            }
                            return storageReference2nd.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                MenuModel menuModel = new MenuModel();
                                menuModel.setNamaMenu(nama);
                                menuModel.setHargaMenu(harga);
                                menuModel.setJenisMenu(jenis);
                                menuModel.setDeskripsiMenu(deskripsi);
                                menuModel.setGambarMenu(task.getResult().toString());
                                databaseReference.push().setValue(menuModel).addOnSuccessListener(TambahMenuActivity.this, new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        onBackPressed();
                                        Toast.makeText(context, "Menu baru ditambah", Toast.LENGTH_LONG).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Menu baru gagal ditambah" + e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    });
                }
            });

        }

    }
    private void pilihGambar(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            pathFile = data.getData();
            try{
                //Ambil dari gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), pathFile);
                setToImageView(getResizedBitmap(bitmap, 1024));
                cekGambar = "ada";
                btnTambah.setEnabled(true);
                btnTambah.setBackground(getResources().getDrawable(R.drawable.onpressbuton));
                btnTambah.setTextColor(getResources().getColor(R.color.hitam));
                btnTambah.setText("Tambah menu");

            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    private void setToImageView (Bitmap bmp){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
        gambarMenu.setImageBitmap(decoded);
    }
    public Bitmap getResizedBitmap (Bitmap image, int maxSize){
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width/ (float) height;
        if (bitmapRatio > 3 ){
            width = maxSize;
            height = (int) (width/bitmapRatio);
        }else{
            height = maxSize;
            width = (int) (height*bitmapRatio);
        }
        return  Bitmap.createScaledBitmap(image, width, height, true);
    }
}
