package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.eatciw.data.ProductDummy;
import com.firebase.eatciw.data.model.Product;

import static com.firebase.eatciw.makanan1.KEY_ITEM;

public class Menu2 extends AppCompatActivity implements ProductAdapter.Interaction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu2);

        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setAdapter(new ProductAdapter(
                this, ProductDummy.getAllDrinks()
        ));
    }

    public void haha(View view) {
        Intent intent = new Intent(Menu2.this, minuman1.class);
        startActivity(intent);
    }

    public void yoyo(View view) {
        Intent intent = new Intent(Menu2.this, minuman2.class);
        startActivity(intent);
    }

    @Override
    public void onClick(Product item) {
        Intent intent = new Intent(Menu2.this, makanan1.class);
        intent.putExtra(KEY_ITEM, item);
        startActivity(intent);
    }
}
