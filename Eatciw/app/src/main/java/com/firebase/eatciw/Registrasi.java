package com.firebase.eatciw;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Registrasi extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword, etNama, noHp;
    Button btDaftar;
    Context context = Registrasi.this;

    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regiss);

        etEmail = findViewById(R.id.user);
        etNama = findViewById(R.id.namaakun);
        btDaftar = findViewById(R.id.btnregis);
        noHp = findViewById(R.id.noHPSaya);
        etPassword = findViewById(R.id.pass);
        databaseReference = FirebaseDatabase.getInstance().getReference();


        btDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String emailFormat = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String password = etPassword.getText().toString();
                String nama = etNama.getText().toString();
                String hp = noHp.getText().toString();
                if(!email.isEmpty() && !password.isEmpty() && !nama.isEmpty() && !hp.isEmpty()){
                    if(password.length() >= 4){
                        if(email.matches(emailFormat)){
                            if(hp.length() >= 10){
                                prosesDaftar(email, password, nama, hp);
                            }else{
                                Toast.makeText(context, "Panjang no hp minimal 10 angka", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Format E-Mail salah!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(context, "Panjang password minimal 4 karakter", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    private void prosesDaftar(String email, String password, String nama, String hp){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final HashMap<String, String> map = new HashMap<>();
        map.put("emailUser", email);
        map.put("noHP", hp);
        map.put("password", password);
        map.put("nama", nama);
        map.put("status", "member");


       DatabaseReference reference = FirebaseDatabase.getInstance().getReference("datauser");
        reference.orderByChild("emailUser").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                        databaseReference.child("datauser").push().setValue(map).addOnSuccessListener(Registrasi.this, new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(context, login.class);
                                        startActivity(intent);
                                        Toast.makeText(context, "Berhasil mendaftar, silahkan masuk", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                }else{
                    dialog.dismiss();
                    Toast.makeText(context, "E-Mail sudah digunakan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent inte = new Intent(Intent.ACTION_MAIN);
        inte.addCategory(Intent.CATEGORY_HOME);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(inte);
    }

}