package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DataDetailBooking extends AppCompatActivity {

    String kodeBooking;
    RelativeLayout tempat1, tempat2;
    TextView tanggal, jenis, kursi, kodeBo;
    Context context = DataDetailBooking.this;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_detail_booking);
        tempat1 = findViewById(R.id.gambar1);
        tempat2 = findViewById(R.id.gambar2);
        tanggal = findViewById(R.id.tanggalPemesanan);
        jenis = findViewById(R.id.tipeBooking);
        kodeBo = findViewById(R.id.kodeBooking);
        kursi = findViewById(R.id.jumlahKursi);

        databaseReference = FirebaseDatabase.getInstance().getReference();


        //proses mengambil data kode booking dari activity sebelumnya
        Intent intent = getIntent();
        kodeBooking = intent.getStringExtra("kodeBooking");

        if(!kodeBooking.isEmpty()){
            dataDetailBooking(kodeBooking);
        }
    }

    private void dataDetailBooking(String kodeBooking){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("databooking").orderByChild("kodeBooking").equalTo(kodeBooking).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    //jika data ada
                    dialog.dismiss();
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                        //jika terdeteksi tipe1(meja vvip)
                        String tempat = dataSnapshot1.child("tempatBooking").getValue().toString();
                        if(tempat.equals("1")){
                            tempat2.setVisibility(View.GONE);
                        }else if(tempat.equals("2")){
                            tempat1.setVisibility(View.GONE);
                        }
                        tanggal.setText(dataSnapshot1.child("tanggalBooking").getValue().toString());
                        jenis.setText(dataSnapshot1.child("tipeBooking").getValue().toString());
                        kursi.setText(dataSnapshot1.child("jumlahKursi").getValue().toString());
                        kodeBo.setText("Kode Booking: " + dataSnapshot1.child("kodeBooking").getValue().toString());
                    }
                }else{
                    dialog.dismiss();
                    Toast.makeText(context, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Data gagal ditemukan " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
