package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.firebase.eatciw.data.prefs.AppPreferenceHelper;

public class promo1 extends AppCompatActivity {

    private AppPreferenceHelper appPreferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo1);

        appPreferenceHelper = new AppPreferenceHelper(getApplicationContext());
    }

    public void setPromo(View view) {
        appPreferenceHelper.setPromo(1);
        Toast.makeText(getApplicationContext(), "Promo dipilih", Toast.LENGTH_SHORT).show();
        finish();
    }
}
