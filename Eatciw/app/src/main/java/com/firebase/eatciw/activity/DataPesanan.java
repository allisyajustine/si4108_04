package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.firebase.eatciw.adapter.DaftarPesananAdapter;
import com.firebase.eatciw.model.PesananModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DataPesanan extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DaftarPesananAdapter daftarPesananAdapter;
    private ArrayList<PesananModel> pesananList;
    private DatabaseReference databaseReference;
    private LinearLayoutManager linearLayoutManager;
    private Context context = DataPesanan.this;
    private TextView judulAtas;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_pesanan);
        recyclerView = findViewById(R.id.recycler);
        judulAtas = findViewById(R.id.topTitle);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        pesananList = new ArrayList<>();
        daftarPesananAdapter = new DaftarPesananAdapter(context, pesananList);
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(daftarPesananAdapter);

        Intent intent = getIntent();//Terima string dari myaccountactivity
        String intetOrder = intent.getStringExtra("tipeOrder");

        switch (intetOrder){
            case "admin":
                dataPesananAll();
                break;
            case "customer":
                //ambil key user yg login/customer
                SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
                String key = sharedPreferences.getString("key", "");
                dataPesananCustomer(key);//tampilkan data pesanan berdasarkan user yang login
                judulAtas.setText("Pesanan Saya");
                break;
        }

    }

    private void dataPesananCustomer(final String key){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("datapesanan").orderByChild("keyPemesan").equalTo(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e("Jumlah data", dataSnapshot.getChildrenCount() + "" );
                pesananList.clear();
                for(DataSnapshot listSnapshot : dataSnapshot.getChildren()){
                    String idPesanan = listSnapshot.getKey();
                    String keyPemesan = listSnapshot.child("keyPemesan").getValue().toString();
                    String namaPemesan = listSnapshot.child("namaPemesan").getValue().toString();
                    String hargaTotal = listSnapshot.child("totalHarga").getValue().toString();
                    String deskripsiPesanan = listSnapshot.child("deskripsiPesanan").getValue().toString();
                    String alamatPemesanan = listSnapshot.child("alamatPemesanan").getValue().toString();
                    String statusPesanan = listSnapshot.child("statusPesanan").getValue().toString();

                    PesananModel pesananModel = new PesananModel();
                    pesananModel.setDeskripsiPesanan(deskripsiPesanan);
                    pesananModel.setHargaTotal(hargaTotal);
                    pesananModel.setIdPesanan(idPesanan);
                    pesananModel.setNamaPemesan(namaPemesan);
                    pesananModel.setKeyPemesan(keyPemesan);
                    pesananModel.setStatusPesanan(statusPesanan);
                    pesananModel.setAlamatPemesanan(alamatPemesanan);
                    pesananModel.setTipePesanan("tidak");
                    pesananList.add(pesananModel);
                }
                dialog.dismiss();
                daftarPesananAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void dataPesananAll(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("datapesanan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pesananList.clear();
                for(DataSnapshot listSnapshot : dataSnapshot.getChildren()){
                    PesananModel pesananModel = new PesananModel();
                    pesananModel.setDeskripsiPesanan(listSnapshot.child("deskripsiPesanan").getValue().toString());
                    pesananModel.setHargaTotal(listSnapshot.child("totalHarga").getValue().toString());
                    pesananModel.setIdPesanan(listSnapshot.getKey());
                    pesananModel.setNamaPemesan(listSnapshot.child("namaPemesan").getValue().toString());
                    String alamatPemesanan = listSnapshot.child("alamatPemesanan").getValue().toString();
                    pesananModel.setKeyPemesan(listSnapshot.child("keyPemesan").getValue().toString());
                    pesananModel.setStatusPesanan(listSnapshot.child("statusPesanan").getValue().toString());
                    pesananModel.setTipePesanan("iya");
                    pesananModel.setAlamatPemesanan(alamatPemesanan);
                    pesananList.add(pesananModel);
                }
                dialog.dismiss();
                daftarPesananAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
