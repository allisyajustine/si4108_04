package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.eatciw.BookingTempat;

public class BookingTempat2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_tempat2);

    }

    public void Batalkan(View view) {
        Intent intent = new Intent(BookingTempat2.this, BookingTempat.class);
        startActivity(intent);
    }

    public void Book(View view) {
        Intent intent = new Intent(BookingTempat2.this, HistoryBooking.class);
        startActivity(intent);
    }
}
