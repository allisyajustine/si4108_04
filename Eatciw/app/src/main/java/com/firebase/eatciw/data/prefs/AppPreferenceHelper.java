package com.firebase.eatciw.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferenceHelper {

    private SharedPreferences preferences;

    private String key = "promo";

    public AppPreferenceHelper(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setPromo(int promo) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, promo);
        editor.apply();
    }

    public int getPromo() {
        return preferences.getInt(key, 0);
    }

    public void resetPromo() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, 0);
        editor.apply();
    }
}
