package com.firebase.eatciw;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class login extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword;
    Button btLogin;
    DatabaseReference databaseReference;
    Context context = login.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = findViewById(R.id.user1);
        etPassword = findViewById(R.id.pass1);
        btLogin = findViewById(R.id.btnlogin);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                if(!email.isEmpty() && !password.isEmpty()){
                    prosesLogin(email, password);
                }else{

                }
            }
        });
    }

    private void prosesLogin(final String email, final String password){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("datauser");
        reference.orderByChild("emailUser").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        String pass2 = dataSnapshot1.child("password").getValue().toString();
                        if(pass2.equals(password)){
                            dialog.dismiss();
                            String status = dataSnapshot1.child("status").getValue().toString();
                            SharedPreferences shared = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            String key = dataSnapshot1.getKey();
                            editor.putString("email", email);
                            editor.putString("status", status);
                            editor.putString("key", key);
                            editor.apply();
                            Intent intent = new Intent(context, Home.class);
                            startActivity(intent);
                            Toast.makeText(context, "Login berhasil", Toast.LENGTH_SHORT).show();
                        }else{
                            dialog.dismiss();
                            Toast.makeText(context, "E-Mail atau password salah!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    dialog.dismiss();
                    Toast.makeText(context, "E-Mail atau password salah!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void regis(View view) {
        Intent intent = new Intent(login.this, Registrasi.class);
        startActivity(intent);
    }

    public void forgotPassword(View view) {
        Intent intent = new Intent(login.this, LupaPassword.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent inte = new Intent(Intent.ACTION_MAIN);
        inte.addCategory(Intent.CATEGORY_HOME);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(inte);
    }
}
