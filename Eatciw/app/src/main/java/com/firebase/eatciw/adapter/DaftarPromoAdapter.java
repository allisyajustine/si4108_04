package com.firebase.eatciw.adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.firebase.eatciw.model.PesananModel;
import com.firebase.eatciw.model.PromoModel;
import com.firebase.eatciw.util.DarkMode;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DaftarPromoAdapter extends RecyclerView.Adapter<DaftarPromoAdapter.PromoViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<PromoModel>promoList;
    private Context context;
    private SimpleDateFormat simpleDateFormat;
    private DatePickerDialog datePickerDialog;

    public DaftarPromoAdapter(Context context, ArrayList<PromoModel>promoList){
        inflater = LayoutInflater.from(context);
        this.promoList = promoList;
        this.context = context;
    }

    @Override
    public PromoViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view  = inflater.inflate(R.layout.data_promo, parent, false);
        PromoViewHolder holder = new PromoViewHolder(view);
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder (final PromoViewHolder holder, final int position) {
        @SuppressLint("DefaultLocale") final String hargaFormat = String.format("%,d", Integer.parseInt(promoList.get(position).getHartaPemotonganPromo()));
        String hargaPemotongan = hargaFormat.replace(",", ".");

        final PromoModel promoModel = promoList.get(position);

        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        final String statusUser = sharedPreferences.getString("status", "");
        String statusDark = sharedPreferences.getString("statusDark", "");
        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(context, holder.itemView, statusDark);

        if(statusDark.equals("true")){
            holder.btnCopy.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_content_copy_white_24dp), null);
        }else{
            holder.btnCopy.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_content_copy_black_24dp), null);
        }

        holder.tanggalPromo.setText(promoModel.getTanggalPromo());
        holder.hargaPemotongan.setText("Rp. " + hargaPemotongan);
        holder.kodePromo.setText(promoModel.getKodePromo());
        holder.statusPromo.setText(promoModel.getStatusPromo());

        holder.btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String kode = promoModel.getKodePromo();
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("kode", kode);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, "Kode promo disalin", Toast.LENGTH_SHORT).show();
            }
        });


        holder.btnRincian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogPromo = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogPromo.setContentView(R.layout.dialog_rincian_promo);
                dialogPromo.setCancelable(true);
                dialogPromo.show();

                TextView rincianPromo = dialogPromo.findViewById(R.id.textRincian);
                TextView btnEdit = dialogPromo.findViewById(R.id.btnUpdate);
                TextView btnHapus=  dialogPromo.findViewById(R.id.btnDelete);
                rincianPromo.setText(promoModel.getDeskripsiPromo());

                //menghilangkan tombol update dan delete jika dia bukan admin
                if(!statusUser.equals("admin")){
                    btnEdit.setVisibility(View.GONE);
                    btnHapus.setVisibility(View.GONE);
                }else{
                    btnEdit.setVisibility(View.VISIBLE);
                    btnHapus.setVisibility(View.VISIBLE);
                }

                //Fungsi edit
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fungsiEdit(dialogPromo, promoModel, promoModel.getIdPromo());
                    }
                });

                btnHapus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialogKonfirmasi = new Dialog(context, R.style.MyAlertDialogTheme);
                        dialogKonfirmasi.setCancelable(true);
                        dialogKonfirmasi.setContentView(R.layout.dialog_konfirmasi);
                        dialogKonfirmasi.show();

                        TextView textTitle = dialogKonfirmasi.findViewById(R.id.messageTitle);
                        TextView textMessage = dialogKonfirmasi.findViewById(R.id.messageDialog);
                        TextView yesbt = dialogKonfirmasi.findViewById(R.id.btnYes);
                        TextView nobt = dialogKonfirmasi.findViewById(R.id.btnBatal);

                        textTitle.setText("Konfirmasi");
                        textMessage.setText("Hapus promo ini?");
                        yesbt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogKonfirmasi.dismiss();
                                hapusPromo(promoModel.getIdPromo(), dialogPromo);
                            }
                        });
                        nobt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogKonfirmasi.dismiss();
                            }
                        });
                    }
                });
            }
        });


    }

    private void hapusPromo(String idPromo, final Dialog dialogPromo){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("datapromo").child(idPromo).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogPromo.dismiss();
                Toast.makeText(context, "Data promo dihapus", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(context, "Gagal menghapus "+ e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("SimpleDateFormat")
    private void fungsiEdit(Dialog dialogPromo, PromoModel promoModel, final String idPromo){
        dialogPromo.dismiss();

        simpleDateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");

        final Dialog dialogEdit = new Dialog(context, R.style.MyAlertDialogTheme);
        dialogEdit.setContentView(R.layout.edit_promo_dialog);
        dialogEdit.setCancelable(true);
        dialogEdit.show();

        final TextView tanggal = dialogEdit.findViewById(R.id.tanggalBerlaku);
        TextView addTgl = dialogEdit.findViewById(R.id.addTanggal);
        final EditText jumlah = dialogEdit.findViewById(R.id.edtJumlah);
        final EditText deskripsi = dialogEdit.findViewById(R.id.edtDeskripsi);
        final Spinner status = dialogEdit.findViewById(R.id.spinnerStatus);
        TextView btnProses = dialogEdit.findViewById(R.id.btnAksi);
        tanggal.setText(promoModel.getTanggalPromo());
        jumlah.setText(promoModel.getHartaPemotonganPromo());
        deskripsi.setText(promoModel.getDeskripsiPromo());

        final Calendar newCal = Calendar.getInstance();
        addTgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(i, i1, i2);
                        tanggal.setText(simpleDateFormat.format(calendar.getTime()));;
                    }
                }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_WEEK));
                datePickerDialog.show();
            }
        });


        btnProses.setText("Edit promo");
        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tanggalTerpilih = tanggal.getText().toString();
                String jumlahPotong = jumlah.getText().toString();
                String deskripsiPromo = deskripsi.getText().toString();
                String statusPromo = status.getSelectedItem().toString();

                if(!tanggalTerpilih.isEmpty() && !jumlahPotong.isEmpty() && !deskripsiPromo.isEmpty()){
                    if(!statusPromo.equals("-Pilih status promo-")){
                        prosesEditPromo(tanggalTerpilih, jumlahPotong, deskripsiPromo, statusPromo, idPromo, dialogEdit);
                    }else{
                        Toast.makeText(context, "Mohon tentukan status promo", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void prosesEditPromo(String tanggal, String jumlahPotong, String des, String statusPr, String idPromo, final Dialog dialogEdit){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datapromo/" + idPromo + "/tanggalBerlaku", tanggal);
        childUpdates.put("/datapromo/" + idPromo + "/jumlahPemotongan", jumlahPotong);
        childUpdates.put("/datapromo/" + idPromo + "/deskripsiPromo", des);
        childUpdates.put("/datapromo/" + idPromo + "/statusPromo", statusPr);

        reference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogEdit.dismiss();
                Toast.makeText(context, "Data promo diupdate", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount(){
        return promoList.size();
    }

    static class PromoViewHolder extends  RecyclerView.ViewHolder{
        TextView tanggalPromo, hargaPemotongan, kodePromo, btnCopy, btnRincian, statusPromo;
        private PromoViewHolder(View view){
            super(view);
            tanggalPromo = view.findViewById(R.id.tanggalBerlaku);
            hargaPemotongan = view.findViewById(R.id.jumlahPemotongan);
            kodePromo = view.findViewById(R.id.kodePromo);
            btnCopy = view.findViewById(R.id.copyText);
            statusPromo = view.findViewById(R.id.statusPromo);
            btnRincian = view.findViewById(R.id.btnRincian);
        }
    }
}
