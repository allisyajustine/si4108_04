package com.firebase.eatciw.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.firebase.eatciw.R;

public class DarkMode {

    public void findLinear(Context context, View v, String statusDark){
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    findLinear(context, child, statusDark);
                }
            } else if (v instanceof TextView) {
                gantiDarkModeText(context, ((TextView) v), statusDark);
            }

            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    findLinear(context, child, statusDark);
                }
                gantiDarkModeLinear(context, ((ViewGroup) v), statusDark);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void gantiDarkModeLinear(Context context, ViewGroup linearLayout, String statusDark){
        if(statusDark.equals("true")){
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.darkcolor));
        }else if(statusDark.equals("false")){
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.putih));
        }

    }
    private void gantiDarkModeText(Context context, TextView textView, String statusDark){
        if(statusDark.equals("true")){
            textView.setTextColor(context.getResources().getColor(R.color.putih));
        }else if(statusDark.equals("false")){
            textView.setTextColor(context.getResources().getColor(R.color.darkcolor));

        }

    }
}
