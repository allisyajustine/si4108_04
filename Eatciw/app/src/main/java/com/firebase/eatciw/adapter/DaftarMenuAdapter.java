package com.firebase.eatciw.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.firebase.eatciw.activity.EditGambarActivity;
import com.firebase.eatciw.activity.EditMenuActivity;
import com.firebase.eatciw.model.MenuModel;
import com.firebase.eatciw.util.DarkMode;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DaftarMenuAdapter extends RecyclerView.Adapter<DaftarMenuAdapter.MenuViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<MenuModel>menuList;
    private Context context;

    public DaftarMenuAdapter(Context context, ArrayList<MenuModel>menuList){
        inflater = LayoutInflater.from(context);
        this.menuList = menuList;
        this.context = context;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view  = inflater.inflate(R.layout.data_menu, parent, false);
        MenuViewHolder holder = new MenuViewHolder(view);
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder (final MenuViewHolder holder, final int position){
        @SuppressLint("DefaultLocale") final String hargaFormat = String.format("%,d", Integer.parseInt(menuList.get(position).getHargaMenu()));
        String hargaReal = hargaFormat.replace(",", ".");

        holder.namaMenu.setText(menuList.get(position).getNamaMenu());
        holder.hargaMenu.setText("Rp. " + hargaReal);
        holder.jenisMenu.setText(menuList.get(position).getJenisMenu());
        Picasso.get().load(menuList.get(position).getGambarMenu()).into(holder.gambarMenu);

        //Mengecek status dark mode di sharedpreferences
        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String statusDark = sharedPreferences.getString("statusDark", "");
        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(context, holder.itemView, statusDark);

        final String idMenu = menuList.get(position).getIdMenu();

        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogKonfirmasi = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogKonfirmasi.setCancelable(true);
                dialogKonfirmasi.setContentView(R.layout.dialog_konfirmasi);
                dialogKonfirmasi.show();

                TextView textTitle = dialogKonfirmasi.findViewById(R.id.messageTitle);
                TextView textMessage = dialogKonfirmasi.findViewById(R.id.messageDialog);
                TextView yesbt = dialogKonfirmasi.findViewById(R.id.btnYes);
                TextView nobt = dialogKonfirmasi.findViewById(R.id.btnBatal);

                textTitle.setText("Konfirmasi");
                textMessage.setText("Hapus menu makanan ini?");
                yesbt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogKonfirmasi.dismiss();
                        hapusMenu(idMenu);
                    }
                });
                nobt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogKonfirmasi.dismiss();
                    }
                });
            }
        });

        holder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogUbah = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogUbah.setCancelable(true);
                dialogUbah.setContentView(R.layout.edit_menu);
                dialogUbah.show();

                final EditText namaMenu = dialogUbah.findViewById(R.id.edtNamaMenu);
                final EditText  hargaMenu = dialogUbah.findViewById(R.id.edtHarga);
                final EditText deskripsiMenu = dialogUbah.findViewById(R.id.edtDeskripsi);
                final Spinner jenisMenu = dialogUbah.findViewById(R.id.edtJenis);
                TextView btnUpdate = dialogUbah.findViewById(R.id.btnEditMenu);
                TextView btnUGambar = dialogUbah.findViewById(R.id.btnEditGambar);

                btnUGambar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogUbah.dismiss();
                        Intent intent = new Intent(context, EditGambarActivity.class);
                        intent.putExtra("gambarUrl", menuList.get(position).getGambarMenu());
                        intent.putExtra("idMenu", idMenu);
                        context.startActivity(intent);
                    }
                });

                namaMenu.setText(menuList.get(position).getNamaMenu());
                hargaMenu.setText(menuList.get(position).getHargaMenu());
                deskripsiMenu.setText(menuList.get(position).getDeskripsiMenu());

                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String nama = namaMenu.getText().toString().trim();
                        String harga = hargaMenu.getText().toString().trim();
                        String deskripsi = deskripsiMenu.getText().toString().trim();
                        String jenis = jenisMenu.getSelectedItem().toString();

                        if(!nama.isEmpty() && !harga.isEmpty( )&& !deskripsi.isEmpty()){
                            if(!jenis.equals("-Pilih jenis baru-")){
                                if(nama.length() >= 4){
                                    if(harga.length() >= 4){
                                        if(deskripsi.length() >= 18){
                                            prosesUpdate(idMenu, nama, harga, deskripsi, jenis, dialogUbah);
                                        }else{
                                            Toast.makeText(context, "Panjang deskripsi minimal 18 karakter", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(context, "Minimal harga minimal Rp. 1.000", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(context, "Panjang nama minimal 4 karakter", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Mohon pilih jenis baru", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

    }

    private void hapusMenu(String idMenu){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/menu/" + idMenu + "/jenisMenu", "dihapus");

        reference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                Toast.makeText(context, "Menu berhasil dihapus", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void prosesUpdate(String idMenu, String nama, String harga, String deskripsi, String jenis, final Dialog dialogUbah){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/menu/" + idMenu + "/namaMenu", nama);
        childUpdates.put("/menu/" + idMenu + "/hargaMenu", harga);
        childUpdates.put("/menu/" + idMenu + "/deskripsiMenu", deskripsi);
        childUpdates.put("/menu/" + idMenu + "/jenisMenu", jenis);
        reference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogUbah.dismiss();
                Toast.makeText(context, "Menu berhasil diupdate", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount(){
        return menuList.size();
    }

    static class MenuViewHolder extends  RecyclerView.ViewHolder{
        TextView namaMenu, hargaMenu, jenisMenu, editBtn, deleteBtn;
        ImageView gambarMenu;
        private MenuViewHolder(View view){
            super(view);
            namaMenu = view.findViewById(R.id.menuNama);
            hargaMenu = view.findViewById(R.id.menuHarga);
            jenisMenu = view.findViewById(R.id.menuJenis);
            deleteBtn = view.findViewById(R.id.deleteMenu);
            gambarMenu = view.findViewById(R.id.menuGambar);
            editBtn = view.findViewById(R.id.btnEdit);
        }
    }
}
