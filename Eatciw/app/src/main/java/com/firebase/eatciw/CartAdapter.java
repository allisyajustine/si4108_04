package com.firebase.eatciw;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.eatciw.adapter.DaftarPesananAdapter;
import com.firebase.eatciw.data.model.Item;
import com.firebase.eatciw.util.GantiFont;

import java.util.List;
import java.util.Locale;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    private List<Item> items;
    Context context;

    public CartAdapter(Context context, List<Item> items) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CartViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_cart,
                parent,
                false
        ));


    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        holder.bind(items.get(position));
        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String tipeFont = sharedPreferences.getString("tipeFont", "");
        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(context , holder.itemView, tipeFont);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class CartViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvCount;
        private TextView tvPrice;

        CartViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvPrice = itemView.findViewById(R.id.tvPrice);
        }

        void bind(Item item) {
            tvName.setText(item.getProduct().getName());
            tvCount.setText(String.valueOf(item.getCount()));
            int price = item.getProduct().getPrice() * item.getCount();
            tvPrice.setText(
                    String.format(Locale.getDefault(), "%,d", price)
            );

        }
    }
}
