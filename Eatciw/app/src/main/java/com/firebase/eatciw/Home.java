package com.firebase.eatciw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.firebase.eatciw.activity.AdminPageActivity;
import com.firebase.eatciw.activity.AkunSaya;
import com.firebase.eatciw.fragment.HomeFragment;
import com.firebase.eatciw.fragment.KeranjangFragment;
import com.firebase.eatciw.fragment.MyAccountFragment;
import com.firebase.eatciw.fragment.PromoFragment;
import com.firebase.eatciw.util.DarkMode;
import com.firebase.eatciw.util.GantiFont;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

public class Home extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getFragmentPage(new HomeFragment());

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_sheet);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;

                switch (item.getItemId()){
                    case R.id.home_menu:
                        fragment = HomeFragment.newInstance();
                        break;
                    case R.id.promo:
                        fragment = PromoFragment.newInstance();
                        break;
                    case R.id.myakun:
                        fragment = MyAccountFragment.newInstance();
                        break;
                }
                getFragmentPage(fragment);
                return true;
            }
        });


        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String tipeFont = sharedPreferences.getString("tipeFont", "");
        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(Home.this , bottomNavigationView, tipeFont);
        String statusDark = sharedPreferences.getString("statusDark", "");
        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(Home.this, bottomNavigationView, statusDark);

    }

    private boolean getFragmentPage(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.page_container, fragment)
                    .commit();

            return true;
        }
        return false;
    }

    public void goToCart(View view){
        Intent intent = new Intent(Home.this, CartActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       Intent inte = new Intent(Intent.ACTION_MAIN);
        inte.addCategory(Intent.CATEGORY_HOME);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(inte);
    }
}

