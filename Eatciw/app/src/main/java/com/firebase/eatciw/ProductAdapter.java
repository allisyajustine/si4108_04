package com.firebase.eatciw;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.eatciw.data.model.Product;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Product> products;
    private Interaction interaction;

    ProductAdapter(Interaction interaction, List<Product> products) {
        this.products = products;
        this.interaction = interaction;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_product,
                parent,
                false
        ), interaction);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivImage;
        private TextView tvName;

        private Interaction interaction;

        ProductViewHolder(@NonNull View itemView, Interaction interaction) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            tvName = itemView.findViewById(R.id.tvName);
            this.interaction = interaction;
        }

        void bind(final Product item) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    interaction.onClick(item);
                }
            });
            ivImage.setImageResource(item.getImageSrc());
            tvName.setText(item.getName());
        }
    }

    interface Interaction {
        void onClick(Product item);
    }
}
