package com.firebase.eatciw.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.firebase.eatciw.R;

public class GantiFont {

    public void findTextView(Context context, View v, String tipeFont){
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    findTextView(context, child, tipeFont);
                }
            } else if (v instanceof TextView) {
                gantiStyleTextView(context, ((TextView) v), tipeFont);
            }

            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    findTextView(context, child, tipeFont);
                }
            } else if (v instanceof Button) {
                gantiTextStyleButton(context, ((Button) v), tipeFont);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gantiTextStyleButton(Context context, Button textView, String tipeFont){
        if(tipeFont.equals("2")){
            //font strawberry
            Typeface typeface = ResourcesCompat.getFont(context, R.font.sweety);
            textView.setTypeface(typeface);
        }else if (tipeFont.equals("1")){
            //Default font
            Typeface typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
            textView.setTypeface(typeface);
        }else if (tipeFont.equals("3")){
            //Bubble bobble
            Typeface typeface = ResourcesCompat.getFont(context, R.font.buble);
            textView.setTypeface(typeface);
        }else if (tipeFont.equals("4")){
            //Perfect blabla font
            Typeface typeface = ResourcesCompat.getFont(context, R.font.perfect);
            textView.setTypeface(typeface);
        }

    }

    private void gantiStyleTextView(Context context, TextView textView, String tipeFont){

        if(tipeFont.equals("2")){
            //font strawberry
            Typeface typeface = ResourcesCompat.getFont(context, R.font.sweety);
            textView.setTypeface(typeface);
        }else if (tipeFont.equals("1")){
            //Default font
            Typeface typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
            textView.setTypeface(typeface);
        }else if (tipeFont.equals("3")){
            //Bubble bobble
            Typeface typeface = ResourcesCompat.getFont(context, R.font.buble);
            textView.setTypeface(typeface);
        }else if (tipeFont.equals("4")){
            //Perfect blabla font
            Typeface typeface = ResourcesCompat.getFont(context, R.font.perfect);
            textView.setTypeface(typeface);
        }

    }
}
