package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

public class BookingActivity extends AppCompatActivity {

    EditText formCariBooking;
    ImageView tempat1, tempat2;
    TextView btnCari;
    Context context = BookingActivity.this;
    DatePickerDialog datePickerDialog;
    SimpleDateFormat simpleDateFormat;
    DatabaseReference databaseReference;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        formCariBooking = findViewById(R.id.edtKode);
        tempat1 = findViewById(R.id.gambar1);
        tempat2 = findViewById(R.id.gambar2);
        btnCari = findViewById(R.id.btnCari);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        simpleDateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");


        //proses mencari data booking
        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String kode = formCariBooking.getText().toString().toUpperCase();
                if(!kode.isEmpty()){
                    prosesCariDetailBooking(kode);
                }else{
                    Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Memesan meja vvip
        tempat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogBooking = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogBooking.setContentView(R.layout.data_pesan_booking);
                dialogBooking.setCancelable(true);
                dialogBooking.show();

                final TextView tanggal = dialogBooking.findViewById(R.id.tanggalPemesanan);
                TextView addTgl = dialogBooking.findViewById(R.id.addTanggal);
                final Spinner jKursi = dialogBooking.findViewById(R.id.jumlahKursi);
                final Spinner tipeB = dialogBooking.findViewById(R.id.tipeBooking);
                TextView btnBook = dialogBooking.findViewById(R.id.btnProses);

                tanggal.setText(null);
                final Calendar newCal = Calendar.getInstance();
                addTgl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(i, i1, i2);
                                tanggal.setText(simpleDateFormat.format(calendar.getTime()));;
                            }
                        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_WEEK));
                        datePickerDialog.show();
                    }
                });

                btnBook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String kursi = jKursi.getSelectedItem().toString();
                        String tipe = tipeB.getSelectedItem().toString();
                        String tgl = tanggal.getText().toString();

                        if(!tgl.isEmpty()){
                            prosesBookingTempat1(tgl, kursi, tipe, dialogBooking);//memproses pemesan meja VVIP
                        }else{
                            Toast.makeText(context, "Mohon pilih tanggal Booking", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });


        //proses pemesanan meja indoor
        tempat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogBooking = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogBooking.setContentView(R.layout.data_pesan_booking);
                dialogBooking.setCancelable(true);
                dialogBooking.show();

                final TextView tanggal = dialogBooking.findViewById(R.id.tanggalPemesanan);
                TextView addTgl = dialogBooking.findViewById(R.id.addTanggal);
                final Spinner jKursi = dialogBooking.findViewById(R.id.jumlahKursi);
                final Spinner tipeB = dialogBooking.findViewById(R.id.tipeBooking);
                TextView btnBook = dialogBooking.findViewById(R.id.btnProses);

                //proses mengambil tanggal dengan datepicker dialog
                final Calendar newCal = Calendar.getInstance();
                addTgl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(i, i1, i2);
                                tanggal.setText(simpleDateFormat.format(calendar.getTime()));;
                            }
                        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_WEEK));
                        datePickerDialog.show();
                    }
                });

                btnBook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String kursi = jKursi.getSelectedItem().toString();
                        String tipe = tipeB.getSelectedItem().toString();
                        String tgl = tanggal.getText().toString();

                        if(!tgl.isEmpty()){
                            prosesBookingTempat2(tgl, kursi, tipe, dialogBooking);//memproses pemesan INDOOR
                        }else{
                            Toast.makeText(context, "Mohon pilih tanggal Booking", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void prosesBookingTempat1(String tgl, String kursi, String tipe, final Dialog dialogBooking){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        //Buat kode booking untuk pengecekan
        final String kodeBooking = UUID.randomUUID().toString().substring(0, 8).toUpperCase();
        HashMap<String, Object> mapPut = new HashMap<>();
        mapPut.put("kodeBooking", kodeBooking);
        mapPut.put("tempatBooking", "1");//Memilih tempat Meja VVIP
        mapPut.put("tanggalBooking", tgl);
        mapPut.put("jumlahKursi", kursi);
        mapPut.put("tipeBooking", tipe);

        databaseReference.child("databooking").push().setValue(mapPut).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogBooking.dismiss();
                Intent intent = new Intent(context, DataDetailBooking.class);
                //memindahkan kode booking random yg sudah dibuat dan mengirim ke activity detail
                intent.putExtra("kodeBooking", kodeBooking);
                startActivity(intent);
                Toast.makeText(context, "Pemesanan tempat berhasil", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(context, "Pemesanan tempat gagal " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void prosesBookingTempat2(String tgl, String kursi, String tipe, final Dialog dialogBooking){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        //Buat kode booking untuk pengecekan
        final String kodeBooking = UUID.randomUUID().toString().substring(0, 8).toUpperCase();
        HashMap<String, Object> mapPut = new HashMap<>();
        mapPut.put("kodeBooking", kodeBooking);
        mapPut.put("tempatBooking", "2");//Memilih tempat INDOOR
        mapPut.put("tanggalBooking", tgl);
        mapPut.put("jumlahKursi", kursi);
        mapPut.put("tipeBooking", tipe);

        databaseReference.child("databooking").push().setValue(mapPut).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogBooking.dismiss();
                Intent intent = new Intent(context, DataDetailBooking.class);
                //memindahkan kode booking random yg sudah dibuat dan mengirim ke activity detail
                intent.putExtra("kodeBooking", kodeBooking);
                startActivity(intent);
                Toast.makeText(context, "Pemesanan tempat berhasil", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(context, "Pemesanan tempat gagal " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void prosesCariDetailBooking(final String kodeBooking){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("databooking").orderByChild("kodeBooking").equalTo(kodeBooking).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    //jika data ada maka akan langsung pergi ke detail
                    dialog.dismiss();
                    Intent intent = new Intent(context, DataDetailBooking.class);
                    intent.putExtra("kodeBooking", kodeBooking);
                    startActivity(intent);
                }else{
                    //jika tidak ditemukan
                    dialog.dismiss();
                    Toast.makeText(context, "Data booking tidak ditemukan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Data gagal ditemukan " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
