package com.firebase.eatciw.data.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;

public class Converters {

    @TypeConverter
    public static String productToString(Product product) {
        Gson gson = new Gson();
        return gson.toJson(product);
    }

    @TypeConverter
    public static Product stringToProduct(String value) {
        return new Gson().fromJson(value, Product.class);
    }
}
