package com.firebase.eatciw.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.eatciw.CartActivity;
import com.firebase.eatciw.CartAdapter;
import com.firebase.eatciw.R;
import com.firebase.eatciw.data.PromoDummy;
import com.firebase.eatciw.data.db.cart.CartDatabase;
import com.firebase.eatciw.data.model.Item;
import com.firebase.eatciw.data.model.Promo;
import com.firebase.eatciw.data.prefs.AppPreferenceHelper;
import com.firebase.eatciw.promo_list;

import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class KeranjangFragment extends Fragment {

    public static KeranjangFragment newInstance() {
        KeranjangFragment fragment = new KeranjangFragment();
        return fragment;
    }

    private Button btnAddPromo;
    private LinearLayout llPromo;
    private TextView tvPromo;
    private TextView tvTotal;

    private AppPreferenceHelper appPreferenceHelper;
    private CartDatabase database;

    private int price = 0;
    private int ongkir = 0;
    private int promo = 0;
    private int total = 0;
    Context context;
    RecyclerView rvCart;
    LinearLayoutManager linearLayoutManager;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_keranjang, container, false);
        rvCart = view.findViewById(R.id.rvCart);
        TextView tvPrice = view.findViewById(R.id.tvPrice);
        TextView tvOngkir = view.findViewById(R.id.tvOngkir);
        btnAddPromo = view.findViewById(R.id.btnAddPromo);
        llPromo = view.findViewById(R.id.llPromo);
        tvPromo = view.findViewById(R.id.tvPromo);
        tvTotal = view.findViewById(R.id.tvTotal);
        Button btnCancel = view.findViewById(R.id.btnCancel);
        Button btnOrder = view.findViewById(R.id.btnOrder);

        appPreferenceHelper = new AppPreferenceHelper(context);
        database = CartDatabase.getInstance(context);

        linearLayoutManager = new LinearLayoutManager(context);

        List<Item> items = database.itemDao().getAll();
        rvCart.setLayoutManager(linearLayoutManager);
        rvCart.setAdapter(new CartAdapter(context, items));

        price = sumPrice(items);
        ongkir = Integer.parseInt(tvOngkir.getText().toString().replace(",",""));

        tvPrice.setText(
                String.format(Locale.getDefault(), "%,d", price)
        );

        btnAddPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, promo_list.class);
                startActivity(intent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.itemDao().deleteAll();
                appPreferenceHelper.resetPromo();
                showConfirmDialog(context);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        int promoId = appPreferenceHelper.getPromo();
        if (promoId == 0) {
            btnAddPromo.setVisibility(View.VISIBLE);
            llPromo.setVisibility(View.GONE);
            promo = 0;
        } else {
            btnAddPromo.setVisibility(View.GONE);
            llPromo.setVisibility(View.VISIBLE);

            List<Promo> promos = PromoDummy.getAllPromos();
            for (Promo promo : promos) {
                if (promo.getId() == promoId) {
                    this.promo = promo.getValue();
                    tvPromo.setText(
                            String.format(Locale.getDefault(), "-%,d", this.promo)
                    );
                    break;
                }
            }
        }

        total = Math.max(price + ongkir - promo, 0);

        tvTotal.setText(
                String.format(Locale.getDefault(), "%,d", total)
        );
    }

    private int sumPrice(List<Item> items) {
        int price = 0;

        for (Item item : items) {
            price += item.getProduct().getPrice() * item.getCount();
        }

        return price;
    }

    @SuppressLint("SetTextI18n")
    private void showConfirmDialog(Context context) {
        View view = View.inflate(context, R.layout.dialog_confirmation, null);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvDescription = view.findViewById(R.id.tvDescription);
        Button btnConfirm = view.findViewById(R.id.btnConfirm);

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();

        tvTitle.setText("Pesanan diterima");
        tvDescription.setText("Pesanan akan dikirim secepatnya\n" +
                "Silakan ditunggu!!\n" +
                "Belanja lagi yaa...");
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.itemDao().deleteAll();
                dialog.dismiss();
                getActivity().finish();
            }
        });

        dialog.show();
    }
}
