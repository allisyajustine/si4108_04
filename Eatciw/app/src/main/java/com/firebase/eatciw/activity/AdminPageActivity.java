package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.firebase.eatciw.adapter.DaftarMenuAdapter;
import com.firebase.eatciw.model.MenuModel;
import com.firebase.eatciw.util.DarkMode;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminPageActivity extends AppCompatActivity {

    TextView addMenu, kembali;
    RecyclerView recyclerView;
    DaftarMenuAdapter daftarMenuAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<MenuModel>menuList;
    Context context = AdminPageActivity.this;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_page);
        kembali = findViewById(R.id.backButton);
        addMenu = findViewById(R.id.btnAddMenu);
        recyclerView = findViewById(R.id.recycler);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference();
        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared,  Context.MODE_PRIVATE);
        String statusDark = sharedPreferences.getString("statusDark", "");
        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(context, getWindow().getDecorView().getRootView(), statusDark);
        menuList = new ArrayList<>();
        daftarMenuAdapter = new DaftarMenuAdapter(context, menuList);
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(daftarMenuAdapter);
        dataMenu();

        addMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TambahMenuActivity.class);
                startActivity(intent);
            }
        });
    }

    private void dataMenu(){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("menu").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dialog.dismiss();
                menuList.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    //Pengambilan data menu dari firebase
                    String nama = dataSnapshot1.child("namaMenu").getValue().toString();
                    String harga = dataSnapshot1.child("hargaMenu").getValue().toString();
                    String jenis = dataSnapshot1.child("jenisMenu").getValue().toString();
                    String deskripsi = dataSnapshot1.child("deskripsiMenu").getValue().toString();
                    String idMenu = dataSnapshot1.getKey();
                    String gambar = dataSnapshot1.child("gambarMenu").getValue().toString();

                    if(!jenis.equals("dihapus")){//tampilkan data yang belum dihapus
                        MenuModel model = new MenuModel();
                        model.setNamaMenu(dataSnapshot1.child("namaMenu").getValue().toString());
                        model.setHargaMenu(dataSnapshot1.child("hargaMenu").getValue().toString());
                        model.setJenisMenu(dataSnapshot1.child("jenisMenu").getValue().toString());
                        model.setIdMenu(dataSnapshot1.getKey());
                        model.setGambarMenu(dataSnapshot1.child("gambarMenu").getValue().toString());
                        model.setDeskripsiMenu(dataSnapshot1.child("deskripsiMenu").getValue().toString());
                        menuList.add(model);
                    }
                }
                daftarMenuAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
