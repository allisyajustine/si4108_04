package com.firebase.eatciw.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.CartActivity;
import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.firebase.eatciw.activity.AdminPageActivity;
import com.firebase.eatciw.activity.DataPesanan;
import com.firebase.eatciw.util.DarkMode;
import com.firebase.eatciw.util.GantiFont;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class MyAccountFragment extends Fragment {

    public static MyAccountFragment newInstance() {
        MyAccountFragment fragment = new MyAccountFragment();
        return fragment;
    }

    private Context context;
    @Override
    public void onAttach(@NonNull Context context){
        super.onAttach(context);
        this.context = context;
    }

    private TextView kembali, emailText, namaText, noHpText, tblEdit, tblEditPas, tblLogout, t1, t2, t3, t4, btnGantiFont;
    private String email, status, nama, noHp;
    private TextView btnAdminPage, btnAdminPageOrder, btnMyOrder;
    private DatabaseReference databaseReference;
    private SharedPreferences sharedPreferences;
    String keyUser, tipeFont, statusDark;
    private SwitchCompat switchCompat;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_my_account, container, false);
        btnAdminPage = view.findViewById(R.id.adminPage);
        btnAdminPageOrder = view.findViewById(R.id.adminPage2);
        btnMyOrder = view.findViewById(R.id.btnMyOrder);
        emailText = view.findViewById(R.id.emailSaya);
        btnGantiFont = view.findViewById(R.id.btnGantiFont);
        switchCompat = view.findViewById(R.id.switchDark);
        namaText = view.findViewById(R.id.namaSaya);
        noHpText = view.findViewById(R.id.noHPSaya);
        tblEdit = view.findViewById(R.id.btnEditProfile);
        tblEditPas = view.findViewById(R.id.btnEditPassword);
        tblLogout = view.findViewById(R.id.btnLogout);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        email  = sharedPreferences.getString("email", "");
        status = sharedPreferences.getString("status", "");
        keyUser  = sharedPreferences.getString("key", "");
        tipeFont = sharedPreferences.getString("tipeFont", "");
        statusDark = sharedPreferences.getString("statusDark", "");

        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(context, view, statusDark);
        if(statusDark.equals("true")){
            switchCompat.setChecked(true);
            darkMode.findLinear(context, view, "true");
        }else{
            darkMode.findLinear(context, view, "false");
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("statusDark", "true");
                    editor.apply();
                    darkMode.findLinear(context, view, "true");
                }else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("statusDark", "false");
                    editor.apply();
                    darkMode.findLinear(context, view, "false");
                }
            }
        });

        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(context, view, tipeFont);



        btnGantiFont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                final Dialog dialogProses = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogProses.setCancelable(true);
                dialogProses.setContentView(R.layout.pilih_font_dialog);
                dialogProses.show();

                final Spinner jenisFont = dialogProses.findViewById(R.id.edtFont);
                TextView prosesBtn = dialogProses.findViewById(R.id.btnProses);
                prosesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view1) {
                        String posisiFont = Integer.toString(jenisFont.getSelectedItemPosition());
                        if(!posisiFont.equals("0")){
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("tipeFont", posisiFont);
                            editor.apply();
                            dialogProses.dismiss();
                            gantiFont.findTextView(context, view, posisiFont);
                        }else{
                            Toast.makeText(context, "Mohon pilih jenis font", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });


        if(status.equals("admin")){
            btnAdminPage.setVisibility(View.VISIBLE);
            btnAdminPageOrder.setVisibility(View.VISIBLE);
        }else{
            btnAdminPage.setVisibility(View.GONE);
            btnAdminPageOrder.setVisibility(View.GONE);
        }
        btnAdminPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AdminPageActivity.class);
                startActivity(intent);
            }
        });

        btnAdminPageOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DataPesanan.class);
                intent.putExtra("tipeOrder", "admin");
                startActivity(intent);
            }
        });

        btnMyOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DataPesanan.class);
                intent.putExtra("tipeOrder", "customer");
                startActivity(intent);
            }
        });

        if(!email.isEmpty()){
            dataAkun(email);
        }
        tblEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogUbah = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogUbah.setCancelable(true);
                dialogUbah.setContentView(R.layout.edit_profile);
                dialogUbah.show();

                final EditText newEm = dialogUbah.findViewById(R.id.edtEmail);
                final EditText newNama = dialogUbah.findViewById(R.id.edtNama);
                final EditText newHP= dialogUbah.findViewById(R.id.edtNoHp);
                newEm.setText(email);
                newNama.setText(nama);
                newHP.setText(noHp);
                TextView tblUpdate = dialogUbah.findViewById(R.id.btnGanti);

                tblUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String nam = newNama.getText().toString();
                        String hp = newHP.getText().toString();
                        String ema = newEm.getText().toString();
                        String emailFormat = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                        if(!ema.isEmpty() && !nam.isEmpty() && !hp.isEmpty()){
                            if(nam.length() >= 4){
                                if(ema.matches(emailFormat)){
                                    if(hp.length() >= 10){
                                        prosesUpdate(ema, hp, nam, dialogUbah);
                                    }else{
                                        Toast.makeText(context, "Panjang no hp minimal 10 angka", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(context, "Format E-Mail salah!", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Panjang nama minimal 4 karakter", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        //proses logout
        tblLogout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                final Dialog dialogKonfirmasi = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogKonfirmasi.setCancelable(true);
                dialogKonfirmasi.setContentView(R.layout.dialog_konfirmasi);
                dialogKonfirmasi.show();
                TextView textTitle = dialogKonfirmasi.findViewById(R.id.messageTitle);
                TextView textMessage = dialogKonfirmasi.findViewById(R.id.messageDialog);
                TextView yesbt = dialogKonfirmasi.findViewById(R.id.btnYes);
                TextView nobt = dialogKonfirmasi.findViewById(R.id.btnBatal);
                textTitle.setText("Yakin");
                textMessage.setText("Keluar dari Eatciw?");
                yesbt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("email", null);
                        editor.putString("status", null);
                        editor.putString("key", null);
                        editor.apply();
                        dialogKonfirmasi.dismiss();
                        Intent intent1 = new Intent(context, MainActivity.class);
                        startActivity(intent1);
                        Toast.makeText(context, "Kamu telah logout", Toast.LENGTH_SHORT).show();

                    }
                });
                nobt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogKonfirmasi.dismiss();
                    }
                });
            }
        });


        tblEditPas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogGanti = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogGanti.setContentView(R.layout.edit_password);
                dialogGanti.setCancelable(true);
                dialogGanti.show();

                final EditText pass = dialogGanti.findViewById(R.id.edtPassword);
                final EditText konfirPas = dialogGanti.findViewById(R.id.edtKonfirmasiPassword);
                final TextView prosesBtn = dialogGanti.findViewById(R.id.btnProses);

                prosesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String newPas = pass.getText().toString();
                        String konfir = konfirPas.getText().toString();

                        if(!newPas.isEmpty() && !konfir.isEmpty()){
                            if(newPas.equals(konfir)){
                                if(newPas.length() >= 4){
                                    prosesGantiPassword(newPas, keyUser, dialogGanti);
                                }else{
                                    Toast.makeText(context, "Panjang password minimal 4 angka", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Konfirmasi password tidak sama", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

        return view;
    }

    private void prosesGantiPassword(String newPas, String key, final Dialog dialogUbah){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datauser/" + key + "/password", newPas);
        databaseReference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogUbah.dismiss();
                Toast.makeText(context, "Password berhasil diganti", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void prosesUpdate(final String ema, String nam, String nHp, final Dialog dialogUbah){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        final String key = sharedPreferences.getString("key", ""); //ambil key dari data login sebelumnya
        final Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datauser/" + key + "/emailUser", ema);
        childUpdates.put("/datauser/" + key + "/nama", nam);
        childUpdates.put("/datauser/" + key + "/noHP", nHp);

        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("datauser");
        reference2.orderByChild("emailUser").equalTo(ema).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    databaseReference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            dialog.dismiss();
                            dialogUbah.dismiss();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email", ema);
                            editor.apply();
                            dataAkun(ema);
                            Toast.makeText(context, "Data diupdate", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    dialog.dismiss();
                    Toast.makeText(context, "E-Mail sudah digunakan", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void dataAkun(final String emailAkun){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("datauser").orderByChild("emailUser").equalTo(emailAkun).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    dialog.dismiss();
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                        nama = dataSnapshot1.child("nama").getValue().toString();
                        noHp = dataSnapshot1.child("noHP").getValue().toString();
                        emailText.setText(emailAkun);
                        namaText.setText(nama);
                        noHpText.setText(noHp);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
