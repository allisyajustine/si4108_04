package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BookingTempat extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = Filter.class.getSimpleName();
    private String mSpinnerLabel = "";

    EditText date_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_tempat);

        Spinner UK = findViewById(R.id.spinner3);
        if (UK != null) {
            UK.setOnItemSelectedListener(this);
        }

        ArrayAdapter<CharSequence> Tipe_Kursi = ArrayAdapter.createFromResource(this, R.array.uk_array, android.R.layout.simple_spinner_item);
        Tipe_Kursi.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        if (UK != null) {
            UK.setAdapter(Tipe_Kursi);
        }

        Spinner TB = findViewById(R.id.spinner4);
        if (TB != null) {
            TB.setOnItemSelectedListener(this);
        }

        ArrayAdapter<CharSequence> Tipe_Booking = ArrayAdapter.createFromResource(this, R.array.tb_array, android.R.layout.simple_spinner_item);
        Tipe_Booking.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        if (TB != null) {
            TB.setAdapter(Tipe_Booking);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mSpinnerLabel = adapterView.getItemAtPosition(i).toString();
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Log.d(TAG, "onNothingSelected: ");
    }

    public void image1(View view) {
        Intent intent = new Intent(BookingTempat.this, BookingTempat2.class);
        startActivity(intent);
    }

    public void image2(View view) {
        Intent intent = new Intent(BookingTempat.this, BookingTempat2.class);
        startActivity(intent);
    }

    public void Date_pick(View view) {
        date_in=findViewById(R.id.editText);

        date_in.setInputType(InputType.TYPE_NULL);

        date_in.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showDateDialog(date_in);
            }
        });
    }

    private void showDateDialog(final EditText date_in) {
        final Calendar calender=Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                String month_string = Integer.toString(month + 1);
                String day_string = Integer.toString(dayOfMonth);
                String year_string= Integer.toString(year);

                calender.set(Calendar.YEAR,year);
                calender.set(Calendar.MONTH,month);
                calender.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                SimpleDateFormat simpleDateformat=new SimpleDateFormat(year_string + "/" + month_string + "/" + day_string);

                date_in.setText(simpleDateformat.format(calender.getTime()));
            }
        };
        new DatePickerDialog(BookingTempat.this,dateSetListener,
                calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH),
                calender.get(Calendar.DAY_OF_MONTH)).show();
    }
}
