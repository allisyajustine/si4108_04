package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.firebase.eatciw.data.db.cart.CartDatabase;
import com.firebase.eatciw.data.model.Item;
import com.firebase.eatciw.data.model.Product;
import com.firebase.eatciw.util.DarkMode;
import com.firebase.eatciw.util.GantiFont;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;

public class makanan1 extends AppCompatActivity {

    public static String KEY_ITEM = "key_item";

    private FloatingActionButton btnCart;
    private ElegantNumberButton numberButton;
    private TextView tvName;
    private TextView tvPrice;
    private TextView tvDescription;
    private ImageView ivImage;

    private CartDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makanan1);

        numberButton = findViewById(R.id.number_button);
        btnCart = findViewById(R.id.btnCart);
        tvName = findViewById(R.id.food_name);
        tvPrice = findViewById(R.id.food_price);
        tvDescription = findViewById(R.id.food_description);
        ivImage = findViewById(R.id.img_food);

        database = CartDatabase.getInstance(getApplicationContext());

        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String tipeFont = sharedPreferences.getString("tipeFont", "");
        String statusDark = sharedPreferences.getString("statusDark", "");
        final DarkMode darkMode = new DarkMode();
        darkMode.findLinear(makanan1.this, getWindow().getDecorView().getRootView(), statusDark);

        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(makanan1.this , getWindow().getDecorView().getRootView(), tipeFont);

        final Product item = getIntent().getParcelableExtra(KEY_ITEM);
        if (item != null) {
            tvName.setText(item.getName());
            tvPrice.setText(
                    String.format(Locale.getDefault(), "%,d", item.getPrice())
            );
            tvDescription.setText(item.getDescription());
            ivImage.setImageResource(item.getImageSrc());
        }

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(numberButton.getNumber());
                database.itemDao().insert(new Item(item, count));

                Toast.makeText(makanan1.this, "Pesanan dimasukan ke dalam keranjang", Toast.LENGTH_SHORT).show();
            }
        });
}
    }

