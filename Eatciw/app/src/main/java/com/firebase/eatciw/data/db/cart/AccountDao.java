package com.firebase.eatciw.data.db.cart;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.firebase.eatciw.data.model.Account;

@Dao
public interface AccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Account account);

    @Query("SELECT * FROM Account WHERE id=:id")
    Account get(Long id);
}
