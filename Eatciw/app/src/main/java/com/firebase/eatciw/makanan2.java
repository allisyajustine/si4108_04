package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.firebase.eatciw.util.GantiFont;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class makanan2 extends AppCompatActivity {

    FloatingActionButton btnCart;
    ElegantNumberButton numberButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makanan2);

        numberButton = findViewById(R.id.number_button);
        btnCart = findViewById(R.id.btnCart);


        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String tipeFont = sharedPreferences.getString("tipeFont", "");
        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(makanan2.this , getWindow().getDecorView().getRootView(), tipeFont);

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(makanan2.this, "Pesanan dimasukan ke dalam keranjang", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
