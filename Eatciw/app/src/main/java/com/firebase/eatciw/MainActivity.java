package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    public static String myShared = "myshared";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        final SharedPreferences sharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        final String email = sharedPreferences.getString("email", "");
        String status = sharedPreferences.getString("status", "");
        String key = sharedPreferences.getString("key", "");
        String tipeFont = sharedPreferences.getString("tipeFont", "");
        String darkStatus = sharedPreferences.getString("statusDark", "");

        if (firebaseAuth.getCurrentUser() != null) {
            Intent intent = new Intent(MainActivity.this, Home.class);
            startActivity(intent);
            finish();
        }
    }
    public void regis1(View view) {
        Intent intent = new Intent(MainActivity.this, Registrasi.class);
        startActivity(intent);
    }
    public void login1(View view) {
        Intent intent = new Intent(MainActivity.this, login.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent inte = new Intent(Intent.ACTION_MAIN);
        inte.addCategory(Intent.CATEGORY_HOME);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(inte);
    }
}
