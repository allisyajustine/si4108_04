package com.firebase.eatciw.data.db.cart;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.firebase.eatciw.data.model.Account;

@Database(entities = {Account.class}, version = 1)
public abstract class AccountDatabase extends RoomDatabase {

    private static AccountDatabase instance;

    public static AccountDatabase getInstance(Context context) {
        synchronized (new Object()) {
            if (instance == null) {
                instance = Room.databaseBuilder(context, AccountDatabase.class, "account")
                        .allowMainThreadQueries()
                        .build();
            }
            return instance;
        }
    }
}
