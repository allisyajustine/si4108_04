package com.firebase.eatciw.data;

import com.firebase.eatciw.data.model.Promo;

import java.util.ArrayList;
import java.util.List;

public class PromoDummy {

    public static List<Promo> getAllPromos() {
        ArrayList<Promo> promos = new ArrayList<>();

        promos.add(new Promo(1, "VALCIW1", 10000));
        promos.add(new Promo(2, "EATKUYS", 20000));
        promos.add(new Promo(3, "FEBCIW", 30000));

        return promos;
    }
}
