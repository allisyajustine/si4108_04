package com.firebase.eatciw.data;

import com.firebase.eatciw.R;
import com.firebase.eatciw.data.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDummy {

    public static List<Product> getAllFoods() {
        ArrayList<Product> foods = new ArrayList<>();

        foods.add(new Product("Nasi Goreng", 15000, "Nasi Goreng telor dadar", R.drawable.nasgor));
        foods.add(new Product("Nasi Bakar", 18000, "Nasi Bakar ayam suir", R.drawable.nasbak));
        foods.add(new Product("Nasi Uduk", 13000, "Nasi Uduk", R.drawable.makanan));
        foods.add(new Product("Nasi Kuning", 12000, "Nasi Kuning", R.drawable.naskun));
        foods.add(new Product("Nasi Kebuli", 15000, "Nasi Kebuli", R.drawable.naskeb));

        return foods;
    }

    public static List<Product> getAllSnacks() {
        ArrayList<Product> snacks = new ArrayList<>();

        snacks.add(new Product("Bakwan Jagung", 10000, "Bakwan Jagung", R.drawable.bakwan));
        snacks.add(new Product("Gehu Pedas", 8000, "Gehu Pedas", R.drawable.gehu));
        snacks.add(new Product("Cireng Isi", 8000, "Cireng Isi", R.drawable.cireng));
        snacks.add(new Product("Donat Kentang", 8000, "Donat Kentang", R.drawable.donat));
        snacks.add(new Product("Kue Pukis", 9000, "Kue Pukis", R.drawable.pukis));

        return snacks;
    }

    public static List<Product> getAllDrinks() {
        ArrayList<Product> drinks = new ArrayList<>();

        drinks.add(new Product("Es Teh", 5000, "Es Teh", R.drawable.esteh));
        drinks.add(new Product("Es Coklat", 12000, "Es Coklat", R.drawable.escoklat));
        drinks.add(new Product("Lemon Squash", 15000, "Lemon Squash", R.drawable.lemon));
        drinks.add(new Product("Orange Squash", 15000, "Orange Squash", R.drawable.orange));
        drinks.add(new Product("Coffee Latte", 12000, "Coffee Latte", R.drawable.coffe));

        return drinks;
    }
}
