package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.eatciw.data.ProductDummy;
import com.firebase.eatciw.data.model.Product;

import static com.firebase.eatciw.makanan1.KEY_ITEM;

public class Menu1 extends AppCompatActivity implements ProductAdapter.Interaction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1);

        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setAdapter(new ProductAdapter(
                this, ProductDummy.getAllFoods()
        ));
    }

    public void wow(View view) {
        Intent intent = new Intent(Menu1.this, makanan1.class);
        startActivity(intent);
    }

    public void wow1(View view) {
        Intent intent = new Intent(Menu1.this, makanan2.class);
        startActivity(intent);
    }

    @Override
    public void onClick(Product item) {
        Intent intent = new Intent(Menu1.this, makanan1.class);
        intent.putExtra(KEY_ITEM, item);
        startActivity(intent);
    }
}
