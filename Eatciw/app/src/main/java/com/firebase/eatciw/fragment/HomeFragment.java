package com.firebase.eatciw.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.firebase.eatciw.BookingTempat;
import com.firebase.eatciw.CartActivity;
import com.firebase.eatciw.Home;
import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.Menu1;
import com.firebase.eatciw.Menu2;
import com.firebase.eatciw.Menu3;
import com.firebase.eatciw.R;
import com.firebase.eatciw.activity.AkunSaya;
import com.firebase.eatciw.activity.BookingActivity;
import com.firebase.eatciw.promo_list;
import com.firebase.eatciw.util.GantiFont;

public class HomeFragment extends Fragment {

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    private Context context;
    private RelativeLayout nex1, nex2, nex3;
    private Button booking;

    String tipeFont;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        nex1 = view.findViewById(R.id.next1);
        nex2 = view.findViewById(R.id.next2);
        nex3 = view.findViewById(R.id.next3);
        booking = view.findViewById(R.id.gegege);
        nex1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
        nex2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next2();
            }
        });
        nex3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next3();
            }
        });
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gegegege();
            }
        });


        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        tipeFont = sharedPreferences.getString("tipeFont", "");
        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(context , view, tipeFont);

        return view;
    }

    public void next() {
        Intent intent = new Intent(context, Menu1.class);
        startActivity(intent);
    }

    public void next2() {
        Intent intent = new Intent(context, Menu2.class);
        startActivity(intent);
    }

    public void next3() {
        Intent intent = new Intent(context, Menu3.class);
        startActivity(intent);
    }

    public void gegegege() {
        Intent intent = new Intent(context, BookingActivity.class);
        startActivity(intent);
    }
}
