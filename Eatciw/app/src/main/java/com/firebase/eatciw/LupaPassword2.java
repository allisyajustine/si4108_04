package com.firebase.eatciw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class LupaPassword2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password2);
    }

    public void Ok(View view) {
        Intent intent = new Intent(LupaPassword2.this, MainActivity.class);
        startActivity(intent);
        ActivityCompat.finishAffinity(LupaPassword2.this);
    }
}
