package com.firebase.eatciw.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.eatciw.R;
import com.firebase.eatciw.model.PesananModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DaftarPesananAdapter extends RecyclerView.Adapter<DaftarPesananAdapter.PesananViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<PesananModel>pesananList;
    private Context context;

    public DaftarPesananAdapter(Context context, ArrayList<PesananModel>pesananList){
        inflater = LayoutInflater.from(context);
        this.pesananList = pesananList;
        this.context = context;
    }

    @Override
    public PesananViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view  = inflater.inflate(R.layout.data_pesanan, parent, false);
        PesananViewHolder holder = new PesananViewHolder(view);
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder (final PesananViewHolder holder, final int position){
        @SuppressLint("DefaultLocale") final String hargaFormat = String.format("%,d", Integer.parseInt(pesananList.get(position).getHargaTotal()));
        String hargaReal = hargaFormat.replace(",", ".");
        holder.hargaTotal.setText("Rp. " + hargaReal);

        final PesananModel model = pesananList.get(position);
        holder.namaPemesan.setText(model.getNamaPemesan());
        holder.statusPesanan.setText(model.getStatusPesanan());
        holder.alamatPemesan.setText(model.getAlamatPemesanan());

        final String idPesanan = model.getIdPesanan();
        final String statusPesan = model.getStatusPesanan();
        String tipePesanan = model.getTipePesanan();//untuk mengecek apa proses validasi admin atau customer

        if(tipePesanan.equals("iya")){
            //proses hapus
            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialogKonfirmasi = new Dialog(context, R.style.MyAlertDialogTheme);
                    dialogKonfirmasi.setCancelable(true);
                    dialogKonfirmasi.setContentView(R.layout.dialog_konfirmasi);
                    dialogKonfirmasi.show();
                    TextView textTitle = dialogKonfirmasi.findViewById(R.id.messageTitle);
                    TextView textMessage = dialogKonfirmasi.findViewById(R.id.messageDialog);
                    TextView yesbt = dialogKonfirmasi.findViewById(R.id.btnYes);
                    TextView nobt = dialogKonfirmasi.findViewById(R.id.btnBatal);
                    textTitle.setText("Konfirmasi");
                    textMessage.setText("Hapus data pesanan ini?");
                    yesbt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogKonfirmasi.dismiss();
                            hapusPesanan(idPesanan);
                        }
                    });
                    nobt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogKonfirmasi.dismiss();
                        }
                    });
                }
            });


            //Melihat rincian
            holder.btnRincian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialogRincian = new Dialog(context, R.style.MyAlertDialogTheme);
                    dialogRincian.setCancelable(true);
                    dialogRincian.setContentView(R.layout.dialog_rincian_admin);
                    dialogRincian.show();

                    TextView textRin = dialogRincian.findViewById(R.id.textRincian);
                    final Spinner pilihanUpda = dialogRincian.findViewById(R.id.spinnerUpdate);
                    LinearLayout goneLine = dialogRincian.findViewById(R.id.goneLinear);
                    textRin.setText(model.getDeskripsiPesanan() + "\nOngkos kirim = Rp. 1.000");
                    TextView btnUpdate = dialogRincian.findViewById(R.id.btnUpdate);
                    if(statusPesan.equals("Selesai")){
                        btnUpdate.setVisibility(View.GONE);
                        goneLine.setVisibility(View.GONE);
                    }
                    btnUpdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String pilihan = pilihanUpda.getSelectedItem().toString();
                            if(!pilihan.equals("-Pilih status-")){
                                updateProses(idPesanan, pilihan, dialogRincian);
                            }else {
                                Toast.makeText(context, "Harap pilih status", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            });
        }else{
            holder.btnDelete.setVisibility(View.GONE);
            holder.btnRincian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialogRincian = new Dialog(context, R.style.MyAlertDialogTheme);
                    dialogRincian.setCancelable(true);
                    dialogRincian.setContentView(R.layout.dialog_rincian_admin);
                    dialogRincian.show();

                    TextView textRin = dialogRincian.findViewById(R.id.textRincian);
                    LinearLayout goneLine = dialogRincian.findViewById(R.id.goneLinear);
                    final Spinner pilihanUpda = dialogRincian.findViewById(R.id.spinnerUpdate);
                    textRin.setText(model.getDeskripsiPesanan() + "Ongkos kirim = Rp. 1.000");
                    TextView btnUpdate = dialogRincian.findViewById(R.id.btnUpdate);
                    goneLine.setVisibility(View.GONE);
                    if(statusPesan.equals("Sedang Dikirim")){
                        btnUpdate.setVisibility(View.VISIBLE);
                    }else{
                        btnUpdate.setVisibility(View.GONE);
                    }
                    btnUpdate.setText("Konfirmasi pesanan");
                    btnUpdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final Dialog dialogKonfirmasi = new Dialog(context, R.style.MyAlertDialogTheme);
                            dialogKonfirmasi.setCancelable(true);
                            dialogKonfirmasi.setContentView(R.layout.dialog_konfirmasi);
                            dialogKonfirmasi.show();
                            TextView textTitle = dialogKonfirmasi.findViewById(R.id.messageTitle);
                            TextView textMessage = dialogKonfirmasi.findViewById(R.id.messageDialog);
                            TextView yesbt = dialogKonfirmasi.findViewById(R.id.btnYes);
                            TextView nobt = dialogKonfirmasi.findViewById(R.id.btnBatal);
                            textTitle.setText("Konfirmasi");
                            textMessage.setText("Konfirmasi pesanan telah kamu terima?");
                            yesbt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogKonfirmasi.dismiss();
                                    prosesPesanan2(idPesanan, dialogRincian);
                                }
                            });
                            nobt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogKonfirmasi.dismiss();
                                }
                            });
                        }
                    });

                }
            });
        }

    }

    private void hapusPesanan(String idPesanan){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("datapesanan").child(idPesanan).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                Toast.makeText(context, "Data pesanan dihapus", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(context, "Gagal menghapus "+ e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void prosesPesanan2(String idPesanan, final Dialog dialogKonfirmasi){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datapesanan/" + idPesanan + "/statusPesanan", "Selesai");
        reference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogKonfirmasi.dismiss();
                Toast.makeText(context, "Pesanan dinyatakan selesai", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateProses(String idPesanan, String pilihan, final Dialog dialogRincian){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datapesanan/" + idPesanan + "/statusPesanan", pilihan);
        reference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogRincian.dismiss();
                Toast.makeText(context, "Status pesanan berubah", Toast.LENGTH_SHORT).show();
            }
        });

    }



    @Override
    public int getItemCount(){
        return pesananList.size();
    }

    static class PesananViewHolder extends  RecyclerView.ViewHolder{
        TextView namaPemesan, hargaTotal, statusPesanan, btnRincian, btnDelete, alamatPemesan;
        private PesananViewHolder(View view){
            super(view);
            namaPemesan = view.findViewById(R.id.namaPemesan);
            alamatPemesan = view.findViewById(R.id.alamatPemesan);
            hargaTotal = view.findViewById(R.id.totalHarga);
            statusPesanan = view.findViewById(R.id.statusPesanan);
            btnRincian = view.findViewById(R.id.btnRincian);
            btnDelete = view.findViewById(R.id.deletePesanan);
        }
    }
}
