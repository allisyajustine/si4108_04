package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.Home;
import com.firebase.eatciw.MainActivity;
import com.firebase.eatciw.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class AkunSaya extends AppCompatActivity {

    TextView kembali, emailText, namaText, noHpText, tblEdit, tblEditPas, tblLogout;
    DatabaseReference databaseReference;
    Context context = AkunSaya.this;
    String email, nama, noHp;
    SharedPreferences sharedPreferences;
    String keyUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akun_saya);
        kembali = findViewById(R.id.backButton);
        emailText = findViewById(R.id.emailSaya);
        namaText = findViewById(R.id.namaSaya);
        noHpText = findViewById(R.id.noHPSaya);
        tblEdit = findViewById(R.id.btnEditProfile);
        tblEditPas = findViewById(R.id.btnEditPassword);
        tblLogout = findViewById(R.id.btnLogout);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference();
        sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        email  = sharedPreferences.getString("email", "");
        keyUser  = sharedPreferences.getString("key", "");
        if(!email.isEmpty()){
            dataAkun(email);
        }
        tblEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogUbah = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogUbah.setCancelable(true);
                dialogUbah.setContentView(R.layout.edit_profile);
                dialogUbah.show();

                final EditText newEm = dialogUbah.findViewById(R.id.edtEmail);
                final EditText newNama = dialogUbah.findViewById(R.id.edtNama);
                final EditText newHP= dialogUbah.findViewById(R.id.edtNoHp);
                newEm.setText(email);
                newNama.setText(nama);
                newHP.setText(noHp);
                TextView tblUpdate = dialogUbah.findViewById(R.id.btnGanti);

                tblUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String nam = newNama.getText().toString();
                        String hp = newHP.getText().toString();
                        String ema = newEm.getText().toString();
                        String emailFormat = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                        if(!ema.isEmpty() && !nam.isEmpty() && !hp.isEmpty()){
                            if(nam.length() >= 4){
                                if(ema.matches(emailFormat)){
                                    if(hp.length() >= 10){
                                        prosesUpdate(ema, hp, nam, dialogUbah);
                                    }else{
                                        Toast.makeText(context, "Panjang no hp minimal 10 angka", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(context, "Format E-Mail salah!", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Panjang nama minimal 4 karakter", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        //proses logout
        tblLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogKonfirmasi = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogKonfirmasi.setCancelable(true);
                dialogKonfirmasi.setContentView(R.layout.dialog_konfirmasi);
                dialogKonfirmasi.show();
                TextView textTitle = dialogKonfirmasi.findViewById(R.id.messageTitle);
                TextView textMessage = dialogKonfirmasi.findViewById(R.id.messageDialog);
                TextView yesbt = dialogKonfirmasi.findViewById(R.id.btnYes);
                TextView nobt = dialogKonfirmasi.findViewById(R.id.btnBatal);
                textTitle.setText("Yakin");
                textMessage.setText("Keluar dari Eatciw?");
                yesbt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("email", null);
                        editor.putString("status", null);
                        editor.putString("key", null);
                        editor.apply();
                        dialogKonfirmasi.dismiss();
                        Intent intent1 = new Intent(context, MainActivity.class);
                        startActivity(intent1);
                        Toast.makeText(context, "Kamu telah logout", Toast.LENGTH_SHORT).show();

                    }
                });
                nobt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogKonfirmasi.dismiss();
                    }
                });
            }
        });


        tblEditPas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogGanti = new Dialog(context, R.style.MyAlertDialogTheme);
                dialogGanti.setContentView(R.layout.edit_password);
                dialogGanti.setCancelable(true);
                dialogGanti.show();

                final EditText pass = dialogGanti.findViewById(R.id.edtPassword);
                final EditText konfirPas = dialogGanti.findViewById(R.id.edtKonfirmasiPassword);
                final TextView prosesBtn = dialogGanti.findViewById(R.id.btnProses);

                prosesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String newPas = pass.getText().toString();
                        String konfir = konfirPas.getText().toString();

                        if(!newPas.isEmpty() && !konfir.isEmpty()){
                            if(newPas.equals(konfir)){
                                if(newPas.length() >= 4){
                                    prosesGantiPassword(newPas, keyUser, dialogGanti);
                                }else{
                                    Toast.makeText(AkunSaya.this, "Panjang password minimal 4 angka", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(AkunSaya.this, "Konfirmasi password tidak sama", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(AkunSaya.this, "Masih ada yang kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

    }

    private void prosesGantiPassword(String newPas, String key, final Dialog dialogUbah){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        final Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datauser/" + key + "/password", newPas);
        databaseReference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                dialogUbah.dismiss();
                Toast.makeText(context, "Password berhasil diganti", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void prosesUpdate(final String ema, String nam, String nHp, final Dialog dialogUbah){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();



        sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        final String key = sharedPreferences.getString("key", ""); //ambil key dari data login sebelumnya
        final Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/datauser/" + key + "/emailUser", ema);
        childUpdates.put("/datauser/" + key + "/nama", nam);
        childUpdates.put("/datauser/" + key + "/noHP", nHp);

        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("datauser");
        reference2.orderByChild("emailUser").equalTo(ema).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    databaseReference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            dialog.dismiss();
                            dialogUbah.dismiss();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email", ema);
                            editor.apply();
                            dataAkun(ema);
                            Toast.makeText(context, "Data diupdate", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    dialog.dismiss();
                    Toast.makeText(context, "E-Mail sudah digunakan", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void dataAkun(final String emailAkun){
        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        databaseReference.child("datauser").orderByChild("emailUser").equalTo(emailAkun).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    dialog.dismiss();
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                        nama = dataSnapshot1.child("nama").getValue().toString();
                        noHp = dataSnapshot1.child("noHP").getValue().toString();
                        emailText.setText(emailAkun);
                        namaText.setText(nama);
                        noHpText.setText(noHp);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
