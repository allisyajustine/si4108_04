package com.firebase.eatciw.data.db.cart;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.firebase.eatciw.data.model.Item;

import java.util.List;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Item item);

    @Query("SELECT * FROM Item ORDER BY id ASC")
    List<Item> getAll();

    @Query("DELETE FROM Item")
    void deleteAll();
}
