package com.firebase.eatciw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class LupaPassword extends AppCompatActivity {

    private EditText etEmail;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);

        etEmail = findViewById(R.id.text_username);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void Kirim(View view) {
        String email = etEmail.getText().toString();
        if (isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Input tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }

        sendPasswordResetEmail(email);
    }

    private void sendPasswordResetEmail(String email) {
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Intent intent = new Intent(LupaPassword.this, LupaPassword2.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Kesalahan", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isEmpty(String value) {
        return value.trim().length() == 0;
    }
}
