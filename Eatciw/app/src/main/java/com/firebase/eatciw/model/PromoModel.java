package com.firebase.eatciw.model;

public class PromoModel {

    private String idPromo;
    private String kodePromo;
    private String tanggalPromo;
    private String statusPromo;
    private String deskripsiPromo;
    private String hartaPemotonganPromo;

    public String getIdPromo() {
        return idPromo;
    }

    public void setIdPromo(String idPromo) {
        this.idPromo = idPromo;
    }

    public String getKodePromo() {
        return kodePromo;
    }

    public void setKodePromo(String kodePromo) {
        this.kodePromo = kodePromo;
    }

    public String getTanggalPromo() {
        return tanggalPromo;
    }

    public void setTanggalPromo(String tanggalPromo) {
        this.tanggalPromo = tanggalPromo;
    }

    public String getStatusPromo() {
        return statusPromo;
    }

    public void setStatusPromo(String statusPromo) {
        this.statusPromo = statusPromo;
    }

    public String getDeskripsiPromo() {
        return deskripsiPromo;
    }

    public void setDeskripsiPromo(String deskripsiPromo) {
        this.deskripsiPromo = deskripsiPromo;
    }

    public String getHartaPemotonganPromo() {
        return hartaPemotonganPromo;
    }

    public void setHartaPemotonganPromo(String hartaPemotonganPromo) {
        this.hartaPemotonganPromo = hartaPemotonganPromo;
    }
}
