package com.firebase.eatciw.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.R;
import com.firebase.eatciw.model.MenuModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditGambarActivity extends AppCompatActivity {

    TextView kembali, btnEdit, btnUpload;
    ImageView gambarMenu;
    DatabaseReference databaseReference;
    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST=1;
    int bitmap_size = 70;
    StorageReference storageReference;
    Uri pathFile;
    String cekGambar;
    Context context = EditGambarActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_gambar);
        kembali = findViewById(R.id.backButton);
        btnEdit = findViewById(R.id.btnEditGambar);
        btnUpload = findViewById(R.id.btnAddGambar);
        gambarMenu = findViewById(R.id.menuGambar);
        btnEdit.setVisibility(View.GONE);
        databaseReference = FirebaseDatabase.getInstance().getReference("menu");
        storageReference = FirebaseStorage.getInstance().getReference();

        Intent intent = getIntent();
        final String idMenu = intent.getStringExtra("idMenu");
        String urlGambar = intent.getStringExtra("gambarUrl");
        if(!idMenu.isEmpty()){
            Picasso.get().load(urlGambar).into(gambarMenu);
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihGambar();
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editGambar(idMenu);
            }
        });
    }


    private void editGambar(final String idMenu){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();

        if (pathFile != null) {
            gambarMenu.setDrawingCacheEnabled(true);
            gambarMenu.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) gambarMenu.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] data = baos.toByteArray();
            final StorageReference storageReference2nd = storageReference.child("menudata/" + System.currentTimeMillis() + "." + "jpg");
            final UploadTask uploadTask =  storageReference2nd.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    dialog.dismiss();
                    Toast.makeText(context, "gagal mengupdate" + exception.getMessage(), Toast.LENGTH_LONG).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();

                            }
                            return storageReference2nd.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                                Map<String, Object> childUpdates = new HashMap<>();
                                childUpdates.put("/menu/" + idMenu + "/gambarMenu", task.getResult().toString());
                                reference.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        dialog.dismiss();
                                        onBackPressed();
                                        Toast.makeText(context, "Gambar berhasil diupdate", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    }

    private void pilihGambar(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            pathFile = data.getData();
            try{
                //Ambil dari gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), pathFile);
                setToImageView(getResizedBitmap(bitmap, 1024));
                cekGambar = "ada";
                btnEdit.setVisibility(View.VISIBLE);

            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    private void setToImageView (Bitmap bmp){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
        gambarMenu.setImageBitmap(decoded);
    }
    public Bitmap getResizedBitmap (Bitmap image, int maxSize){
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width/ (float) height;
        if (bitmapRatio > 3 ){
            width = maxSize;
            height = (int) (width/bitmapRatio);
        }else{
            height = maxSize;
            width = (int) (height*bitmapRatio);
        }
        return  Bitmap.createScaledBitmap(image, width, height, true);
    }
}
