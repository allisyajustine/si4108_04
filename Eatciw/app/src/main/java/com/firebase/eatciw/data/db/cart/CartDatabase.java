package com.firebase.eatciw.data.db.cart;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.firebase.eatciw.data.model.Converters;
import com.firebase.eatciw.data.model.Item;

@Database(entities = {Item.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class CartDatabase extends RoomDatabase {

    public abstract ItemDao itemDao();

    private static CartDatabase instance;

    public static CartDatabase getInstance(Context context) {
        synchronized (new Object()) {
            if (instance == null) {
                instance = Room.databaseBuilder(context, CartDatabase.class, "cart")
                        .allowMainThreadQueries()
                        .build();
            }
            return instance;
        }
    }
}
