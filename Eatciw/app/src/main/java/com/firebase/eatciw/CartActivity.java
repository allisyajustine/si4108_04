package com.firebase.eatciw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.eatciw.data.PromoDummy;
import com.firebase.eatciw.data.db.cart.CartDatabase;
import com.firebase.eatciw.data.model.Item;
import com.firebase.eatciw.data.model.Promo;
import com.firebase.eatciw.data.prefs.AppPreferenceHelper;
import com.firebase.eatciw.util.GantiFont;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class CartActivity extends AppCompatActivity {

    private Button btnAddPromo;
    private LinearLayout llPromo;
    private TextView tvPromo;
    private TextView tvTotal;

    private AppPreferenceHelper appPreferenceHelper;
    private CartDatabase database;

    private int price = 0;
    private int ongkir = 0;
    private int promo = 0;
    private int total = 0;
    private String deskripsi;
    String tipeFont;

    Context context = CartActivity.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        RecyclerView rvCart = findViewById(R.id.rvCart);
        TextView tvPrice = findViewById(R.id.tvPrice);
        TextView tvOngkir = findViewById(R.id.tvOngkir);
        btnAddPromo = findViewById(R.id.btnAddPromo);
        llPromo = findViewById(R.id.llPromo);
        tvPromo = findViewById(R.id.tvPromo);
        tvTotal = findViewById(R.id.tvTotal);
        Button btnCancel = findViewById(R.id.btnCancel);
        Button btnOrder = findViewById(R.id.btnOrder);

        appPreferenceHelper = new AppPreferenceHelper(getApplicationContext());
        database = CartDatabase.getInstance(getApplicationContext());

        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        tipeFont = sharedPreferences.getString("tipeFont", "");
        final GantiFont gantiFont = new GantiFont();
        gantiFont.findTextView(context , getWindow().getDecorView().getRootView(), tipeFont);

        List<Item> items = database.itemDao().getAll();
        rvCart.setAdapter(new CartAdapter(context, items));

        price = sumPrice(items);
        deskripsi = ambilPesanan(items);
        Log.d("Deskripsi pesanan:", deskripsi);
        ongkir = Integer.parseInt(tvOngkir.getText().toString().replace(",",""));

        tvPrice.setText(
                String.format(Locale.getDefault(), "%,d", price)
        );


        tvTotal.setText(
                String.format(Locale.getDefault(), "%,d", price + ongkir)
        );

        btnAddPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogPromo = new Dialog(CartActivity.this, R.style.MyAlertDialogTheme);
                dialogPromo.setCancelable(true);
                dialogPromo.setContentView(R.layout.kode_promo);
                dialogPromo.show();

                final EditText edtKode = dialogPromo.findViewById(R.id.edtPromo);
                TextView proses = dialogPromo.findViewById(R.id.btnProses);

                proses.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String kode = edtKode.getText().toString();
                        if(!kode.isEmpty()){
                            prosesCariPromo(kode, dialogPromo);
                        }else{
                            Toast.makeText(context, "Kode promo tidak boleh kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogProses = new Dialog(CartActivity.this, R.style.MyAlertDialogTheme);
                dialogProses.setCancelable(true);
                dialogProses.setContentView(R.layout.nama_pemesan_dialog);
                dialogProses.show();

                final EditText namaPemesan = dialogProses.findViewById(R.id.edtNama);
                final EditText alamatPemesan = dialogProses.findViewById(R.id.edtAlamat);
                TextView prosesBtn = dialogProses.findViewById(R.id.btnProses);
                prosesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String nama = namaPemesan.getText().toString();
                        String alamat = alamatPemesan.getText().toString();
                        if(nama.length() >= 4){
                            if(alamat.length() >= 10){
                                prosesPesananan(deskripsi, total, nama, alamat);
                            }else{
                                Toast.makeText(context, "Alamat minimal 10 karakter", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(CartActivity.this, "Masukkan nama minimal 4 karakter", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void prosesCariPromo(String kode, final Dialog dialogPromo){
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("datapromo").orderByChild("kodePromo").equalTo(kode).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    dialog.dismiss();
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                        String statusPromo = dataSnapshot1.child("statusPromo").getValue().toString();
                        if(statusPromo.equals("Berlaku")){

                            //Promo berlaku, lakukan pengurangan harga total dengan promo
                            dialogPromo.dismiss();
                            promo = Integer.parseInt(dataSnapshot1.child("jumlahPemotongan").getValue().toString());
                            tvPromo.setText(
                                    String.format(Locale.getDefault(), "-%,d", promo)
                            );
                            btnAddPromo.setVisibility(View.GONE);
                            llPromo.setVisibility(View.VISIBLE);

                            total = price + ongkir - promo;
                            tvTotal.setText(
                                    String.format(Locale.getDefault(), "%,d", total)
                            );
                            Toast.makeText(context, "Promo digunakan", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(context, "Promo sudah tidak berlaku", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    btnAddPromo.setVisibility(View.VISIBLE);
                    llPromo.setVisibility(View.GONE);
                    promo = 0;
                    dialog.dismiss();
                    Toast.makeText(context, "Kode promo tidak ditemukan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void prosesPesananan(String deskripsi, int total, String nama, String alamat){
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.setContentView(R.layout.dialog_progres);
        dialog.setCancelable(false);
        dialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.myShared, Context.MODE_PRIVATE);
        String keyLogin = sharedPreferences.getString("key", "");//mengambil id dari user yang login

        final HashMap<String, String> map = new HashMap<>();
        map.put("deskripsiPesanan", deskripsi);
        map.put("totalHarga", Integer.toString(total));
        map.put("namaPemesan", nama);
        map.put("alamatPemesanan", alamat);
        map.put("statusPesanan", "Menunggu diproses");
        map.put("keyPemesan", keyLogin); //agar nantinya dapat menampilkan data pesanan berdasarkan user yang login

        databaseReference.child("datapesanan").push().setValue(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                database.itemDao().deleteAll();
                appPreferenceHelper.resetPromo();
                showConfirmDialog(CartActivity.this);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(CartActivity.this, "Pesanan gagal diproses " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*int promoId = appPreferenceHelper.getPromo();
        if (promoId == 0) {
            btnAddPromo.setVisibility(View.VISIBLE);
            llPromo.setVisibility(View.GONE);
            promo = 0;
        } else {
            btnAddPromo.setVisibility(View.GONE);
            llPromo.setVisibility(View.VISIBLE);

            List<Promo> promos = PromoDummy.getAllPromos();
            for (Promo promo : promos) {
                if (promo.getId() == promoId) {
                    this.promo = promo.getValue();
                    tvPromo.setText(
                            String.format(Locale.getDefault(), "-%,d", this.promo)
                    );
                    break;
                }
            }
        }*/
    }

    private String ambilPesanan(List<Item> items){
        StringBuilder deskripsi = new StringBuilder();
        for(Item item : items){
            int hargaPerPorsi = item.getProduct().getPrice() * item.getCount();
            deskripsi.append(item.getProduct().getName()).append(" ").append(item.getCount()).append(" porsi = ")
                .append("Rp. ")
                .append(String.format(Locale.getDefault(), "%,d", hargaPerPorsi)).append("\n");
        }
        return deskripsi.toString();
    }

    private int sumPrice(List<Item> items) {
        int price = 0;

        for (Item item : items) {
            price += item.getProduct().getPrice() * item.getCount();
        }

        return price;
    }

    @SuppressLint("SetTextI18n")
    private void showConfirmDialog(Context context) {
        View view = View.inflate(context, R.layout.dialog_confirmation, null);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvDescription = view.findViewById(R.id.tvDescription);
        Button btnConfirm = view.findViewById(R.id.btnConfirm);

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();

        tvTitle.setText("Pesanan diterima");
        tvDescription.setText("Pesanan akan dikirim secepatnya\n" +
                "Silakan ditunggu!!\n" +
                "Belanja lagi yaa...");
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.itemDao().deleteAll();
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }
}
