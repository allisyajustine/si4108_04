package com.firebase.eatciw.model;

public class PesananModel {

    private String namaPemesan;
    private String hargaTotal;
    private String keyPemesan;
    private String idPesanan;
    private String deskripsiPesanan;
    private String statusPesanan;
    private String alamatPemesanan;

    public String getAlamatPemesanan() {
        return alamatPemesanan;
    }

    public void setAlamatPemesanan(String alamatPemesanan) {
        this.alamatPemesanan = alamatPemesanan;
    }

    private String tipePesanan;

    public String getTipePesanan() {
        return tipePesanan;
    }

    public void setTipePesanan(String tipePesanan) {
        this.tipePesanan = tipePesanan;
    }

    public String getNamaPemesan() {
        return namaPemesan;
    }

    public void setNamaPemesan(String namaPemesan) {
        this.namaPemesan = namaPemesan;
    }

    public String getHargaTotal() {
        return hargaTotal;
    }

    public void setHargaTotal(String hargaTotal) {
        this.hargaTotal = hargaTotal;
    }

    public String getKeyPemesan() {
        return keyPemesan;
    }

    public void setKeyPemesan(String keyPemesan) {
        this.keyPemesan = keyPemesan;
    }

    public String getIdPesanan() {
        return idPesanan;
    }

    public void setIdPesanan(String idPesanan) {
        this.idPesanan = idPesanan;
    }

    public String getDeskripsiPesanan() {
        return deskripsiPesanan;
    }

    public void setDeskripsiPesanan(String deskripsiPesanan) {
        this.deskripsiPesanan = deskripsiPesanan;
    }

    public String getStatusPesanan() {
        return statusPesanan;
    }

    public void setStatusPesanan(String statusPesanan) {
        this.statusPesanan = statusPesanan;
    }
}
