package com.firebase.eatciw.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

    private String name;
    private int Price;
    private String description;
    private int imageSrc;

    public Product(String name, int price, String description, int imageSrc) {
        this.name = name;
        Price = price;
        this.description = description;
        this.imageSrc = imageSrc;
    }

    protected Product(Parcel in) {
        name = in.readString();
        Price = in.readInt();
        description = in.readString();
        imageSrc = in.readInt();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(int imageSrc) {
        this.imageSrc = imageSrc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(Price);
        dest.writeString(description);
        dest.writeInt(imageSrc);
    }
}
