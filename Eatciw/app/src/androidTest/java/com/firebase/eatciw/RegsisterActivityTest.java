package com.firebase.eatciw;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class RegsisterActivityTest {
    @Rule
    public ActivityTestRule<Registrasi> nActivity = new ActivityTestRule<>(Registrasi.class);

    private String email = "allisyajstn@gmail.com";
    private String password = "12345678910";
    private String nama = "asya";
    private String hp = "08111180353";

    @Test
    public void emailEditable(){
        onView(withId(R.id.user)).perform(typeText(email), closeSoftKeyboard());
    }

    @Test
    public void passwordEditable(){
        onView(withId(R.id.pass)).perform(typeText(password), closeSoftKeyboard());
    }

    @Test
    public void namaEditable(){
        onView(withId(R.id.namaakun)).perform(typeText(nama), closeSoftKeyboard());
    }

    @Test
    public void hpEditable(){
        onView(withId(R.id.noHPSaya)).perform(typeText(hp), closeSoftKeyboard());
    }
}
